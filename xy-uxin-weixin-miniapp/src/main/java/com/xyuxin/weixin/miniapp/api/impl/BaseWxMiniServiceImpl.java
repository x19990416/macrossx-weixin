/*
 * Copyright (c) 2021, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.miniapp.api.impl;

import com.xyuxin.weixin.common.bean.WxAccessToken;
import com.xyuxin.weixin.common.utils.http.BaseHttpGetExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.miniapp.WxMiniAppConfig;
import com.xyuxin.weixin.miniapp.api.WxMiniAppService;
import com.xyuxin.weixin.miniapp.bean.BaseWxMiniAppCode2Session;
import com.google.gson.Gson;

import java.io.IOException;

/**
 * http 请求方法，带有 accesstoken 校验
 *
 * @author Guo Limin
 */
public class BaseWxMiniServiceImpl implements WxMiniAppService {
    private WxMiniAppConfig config;
    private WxAccessToken wxAccessToken;
    private HttpClientExecutor executorFactory;

    public BaseWxMiniServiceImpl(WxMiniAppConfig config, HttpClientExecutor executorFactory) {
        this.config = config;
        this.executorFactory = executorFactory;
    }

    protected synchronized WxAccessToken getWxAccessToken() throws IOException {
        String url = String.format(WxMiniAppService.GET_ACCESS_TOKEN_URL, config.getAppId(), config.getAppSecret());
        BaseHttpGetExecutor<String> executor = executorFactory.httpGetStringExecutor();
        return new Gson().fromJson(executor.execute(url, null), WxAccessToken.class);
    }

    @Override
    public synchronized String getAccessToken() throws IOException {
        if (this.wxAccessToken == null || this.wxAccessToken.isOutExpire()) {
            this.wxAccessToken = getWxAccessToken();
        }
        return wxAccessToken.getAccessToken();
    }

    @Override
    public BaseWxMiniAppCode2Session code2Session(String code) throws IOException {
        String url = String.format(WxMiniAppService.SESSION_2_CODE, config.getAppId(), config.getAppSecret(), code);
        BaseHttpGetExecutor<String> executor = executorFactory.httpGetStringExecutor();
        return new Gson().fromJson(executor.execute(url, null), BaseWxMiniAppCode2Session.class);
    }
}
