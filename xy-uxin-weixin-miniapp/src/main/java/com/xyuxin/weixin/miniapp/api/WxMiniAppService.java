/*
 * Copyright (c) 2021, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.miniapp.api;

import com.xyuxin.weixin.common.service.WxService;
import com.xyuxin.weixin.miniapp.bean.BaseWxMiniAppCode2Session;

import javax.validation.constraints.NotEmpty;
import java.io.IOException;

/**
 * 微信小程序api接口
 *
 * @author Guo Limin
 */
public interface WxMiniAppService extends WxService {
    /**
     * access token
     *
     * @see <a href='https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/access-token/auth.getAccessToken.html'>接口调用凭证 </a>
     */
    String GET_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";
    /**
     * code2Session
     *
     * @see <a href='https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/login/auth.code2Session.html'> 登录 </a>
     */
    String SESSION_2_CODE = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";

    /**
     * auth.checkEncryptedData(GET)
     */
    String CHECK_ENCRYPTED_DATA = "https://api.weixin.qq.com/wxa/business/checkencryptedmsg?access_token=%s";
    /**
     * auth.getPaidUnionId(GET)
     */
    String GET_PAID_UNION_ID = "https://api.weixin.qq.com/wxa/getpaidunionid?access_token=%s&openid=%s";
    /**
     * auth.getPluginOpenPId(POST)
     */
    String GET_PLUGIN_OPEN_ID = "https://api.weixin.qq.com/wxa/getpluginopenpid?access_token=%S";

    /**
     * code2Session
     *
     * @param code 小程序端传入的code
     * @return WxMiniAppCode2Session 用户信息
     * @throws IOException
     */
    BaseWxMiniAppCode2Session code2Session(@NotEmpty String code) throws IOException;
}
