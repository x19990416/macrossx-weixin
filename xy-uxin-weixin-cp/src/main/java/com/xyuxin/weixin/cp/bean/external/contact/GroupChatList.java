package com.xyuxin.weixin.cp.bean.external.contact;

import com.google.gson.annotations.SerializedName;
import com.xyuxin.weixin.common.bean.WxErr;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ToString(callSuper = true)
public class GroupChatList extends WxErr {
    @SerializedName("group_chat_list")
    private List<GroupChat> groupChatList;
    @SerializedName("next_cursor")
    private String nextCursor;

    @Data
    public static class GroupChat {
        @SerializedName("chat_id")
        private String chatId;
        /**
         * @see com.xyuxin.weixin.cp.api.WxCpExternalContactService.GroupChatListStatus
         */
        @SerializedName("status")
        private Integer status;

    }
}
