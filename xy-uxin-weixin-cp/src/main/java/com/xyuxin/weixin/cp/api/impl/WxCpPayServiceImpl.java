package com.xyuxin.weixin.cp.api.impl;

import com.google.gson.Gson;
import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.cp.WxCpConfig;
import com.xyuxin.weixin.cp.api.WxCpPayService;
import com.xyuxin.weixin.cp.bean.pay.WxCpBillList;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class WxCpPayServiceImpl extends BaseWxCpServiceImpl implements WxCpPayService {
    public WxCpPayServiceImpl(WxCpConfig config, HttpClientExecutor executorFactory) {
        super(config, executorFactory);
    }

    @Override
    public WxCpBillList getBillList(long beginTimeSec, long endTimeSec, String payeeUserId, String cursor, Integer limit) throws IOException {
        String url = String.format(WxCpPayService.POST_BILL_LIST, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        Map<String, Object> data = new HashMap<>();
        data.put("begin_time", beginTimeSec);
        data.put("end_time", endTimeSec);
        data.put( "payee_userid", payeeUserId);
        data.put("cursor", cursor);
        data.put( "limit", limit);
        return new Gson().fromJson(executor.execute(url, new Gson().toJson(data)), WxCpBillList.class);
    }
}
