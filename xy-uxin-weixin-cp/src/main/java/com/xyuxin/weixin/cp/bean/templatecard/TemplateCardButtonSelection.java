package com.xyuxin.weixin.cp.bean.templatecard;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TemplateCardButtonSelection  {
    private String questionKey;
    private String title;
    private String selectedId;
    private List<TemplateCardButtonSelectionOption> optionList;

    /**
     * To json json object.
     *
     * @return the json object
     */
    public JsonObject toJson() {
        JsonObject btnObject = new JsonObject();

        if (StringUtils.isNotBlank(this.questionKey)) {
            btnObject.addProperty("question_key", this.questionKey);
        }
        if (StringUtils.isNotBlank(this.title)) {
            btnObject.addProperty("title", this.title);
        }
        if (StringUtils.isNotBlank(this.selectedId)) {
            btnObject.addProperty("selected_id", this.selectedId);
        }

        if (this.optionList != null && this.optionList.size() > 0) {
            JsonArray optionJsonArray = new JsonArray();
            for (TemplateCardButtonSelectionOption jump : this.getOptionList()) {
                JsonObject tempObject = jump.toJson();
                optionJsonArray.add(tempObject);
            }
            btnObject.add("option_list", optionJsonArray);
        }
        return btnObject;
    }
}

