package com.xyuxin.weixin.cp.bean.user;

import com.google.gson.annotations.SerializedName;
import com.xyuxin.weixin.common.bean.WxErr;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 企业可通过接口批量邀请成员使用企业微信，邀请后将通过短信或邮件下发通知。
 * @author guolimin
 * @see <a href="https://developer.work.weixin.qq.com/document/path/90975">邀请成员</a>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ToString(callSuper = true)
public class WxCpUserInvalidInvite extends WxErr {
    /**
     * 成员ID列表, 最多支持1000个。
     */
    @SerializedName("invaliduser")
    private List<String> invalidUser;
    /**
     * 部门ID列表，最多支持100个。
     */
    @SerializedName("invalidparty")
    private List<String> invalidParty;
    /**
     * 标签ID列表，最多支持100个。
     */
    @SerializedName("invalidtag")
    private List<String> invalidTag;
}
