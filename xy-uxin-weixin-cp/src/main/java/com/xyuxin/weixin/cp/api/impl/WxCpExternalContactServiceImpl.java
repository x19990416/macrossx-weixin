package com.xyuxin.weixin.cp.api.impl;

import com.google.gson.Gson;
import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.cp.WxCpConfig;
import com.xyuxin.weixin.cp.api.WxCpExternalContactService;
import com.xyuxin.weixin.cp.bean.external.contact.GroupChatGet;
import com.xyuxin.weixin.cp.bean.external.contact.GroupChatGetReq;
import com.xyuxin.weixin.cp.bean.external.contact.GroupChatList;
import com.xyuxin.weixin.cp.bean.external.contact.GroupChatListReq;

import java.io.IOException;

public class WxCpExternalContactServiceImpl extends BaseWxCpServiceImpl implements WxCpExternalContactService {

    public WxCpExternalContactServiceImpl(WxCpConfig config, HttpClientExecutor executorFactory) {
        super(config, executorFactory);
    }

    @Override
    public GroupChatList groupChatList(GroupChatListReq request) throws IOException {
        String url = String.format(WxCpExternalContactService.POST_GROUPCHAT_LIST, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        return new Gson().fromJson(executor.execute(url, new Gson().toJson(request)), GroupChatList.class);
    }

    @Override
    public GroupChatGet groupChatGet(GroupChatGetReq request) throws IOException {
        String url = String.format(WxCpExternalContactService.POST_GROUPCHAT_GET, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        return new Gson().fromJson(executor.execute(url, new Gson().toJson(request)), GroupChatGet.class);
    }
}
