package com.xyuxin.weixin.cp.bean.external.contact;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class GroupChatListReq {
    /**
     * @see com.xyuxin.weixin.cp.api.WxCpExternalContactService.GroupChatStatusFilter
     */
    @SerializedName("status_filter")
    private Integer statusFilter = 0;
    @SerializedName("owner_filter")
    private UserIdList ownerFilter;
    private String cursor;
    private Integer limit = 10;

    @Data
    @Accessors(chain = true)

    public static class UserIdList {
        @SerializedName("userid_list")
        List<String> useridList;
    }
}
