package com.xyuxin.weixin.cp.bean.external.contact;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class GroupChatGetReq {
    @SerializedName("chat_id")
    private String chatId;
    @SerializedName("need_name")
    private Integer needName = 1;
}
