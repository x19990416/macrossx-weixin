/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.cp.util.gson;

import com.google.gson.*;
import com.xyuxin.weixin.common.utils.gson.GsonMapper;
import com.xyuxin.weixin.cp.bean.Gender;
import com.xyuxin.weixin.cp.bean.user.WxCpUser;

import java.lang.reflect.Type;
import java.util.Objects;

/**
 * @author Guo Limin
 * @see {WxMpConstants.SubscribeSceneType} 序列化
 */
public class WxCpUserGsonAdapter implements JsonDeserializer<WxCpUser>, JsonSerializer<WxCpUser> {
    private static final String EXTERNAL_PROFILE = "external_profile";
    private static final String EXTERNAL_ATTR = "external_attr";
    private static final String EXTRA_ATTR = "extattr";
    private static final String EXTERNAL_POSITION = "external_position";
    private static final String DEPARTMENT = "department";
    private static final String EXTERNAL_CORP_NAME = "external_corp_name";
    private static final String WECHAT_CHANNELS = "wechat_channels";

    @Override
    public WxCpUser deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject o = json.getAsJsonObject();
        WxCpUser user = new WxCpUser();

        if (o.get(DEPARTMENT) != null) {
            JsonArray departJsonArray = o.get(DEPARTMENT).getAsJsonArray();
            Long[] departIds = new Long[departJsonArray.size()];
            int i = 0;
            for (JsonElement jsonElement : departJsonArray) {
                departIds[i++] = jsonElement.getAsLong();
            }
            user.setDepartIds(departIds);
        }

        if (o.get("order") != null) {
            JsonArray departJsonArray = o.get("order").getAsJsonArray();
            Integer[] orders = new Integer[departJsonArray.size()];
            int i = 0;
            for (JsonElement jsonElement : departJsonArray) {
                orders[i++] = jsonElement.getAsInt();
            }
            user.setOrders(orders);
        }

        if (o.get("positions") != null) {
            JsonArray positionJsonArray = o.get("positions").getAsJsonArray();
            String[] positions = new String[positionJsonArray.size()];
            int i = 0;
            for (JsonElement jsonElement : positionJsonArray) {
                positions[i++] = jsonElement.getAsString();
            }
            user.setPositions(positions);
        }

        user.setUserId(GsonMapper.get(String.class, o.get("userid")));
        user.setName(GsonMapper.get(String.class, o.get("name")));
        user.setPosition(GsonMapper.get(String.class, o.get("position")));
        user.setMobile(GsonMapper.get(String.class, o.get("mobile")));
        user.setGender(Gender.fromCode(GsonMapper.get(String.class, o.get("gender"))));
        user.setEmail(GsonMapper.get(String.class, o.get("email")));
        user.setBizMail(GsonMapper.get(String.class, o.get("biz_mail")));
        user.setAvatar(GsonMapper.get(String.class, o.get("avatar")));
        user.setThumbAvatar(GsonMapper.get(String.class, o.get("thumb_avatar")));
        user.setAddress(GsonMapper.get(String.class, o.get("address")));

        user.setAvatarMediaId(GsonMapper.get(String.class, o.get("avatar_mediaid")));
        user.setStatus(GsonMapper.get(Integer.class, o.get("status")));
        user.setEnable(GsonMapper.get(Integer.class, o.get("enable")));
        user.setAlias(GsonMapper.get(String.class, o.get("alias")));
        user.setIsLeader(GsonMapper.get(Integer.class, o.get("isleader")));
        user.setIsLeaderInDept(Objects.requireNonNull(GsonMapper.getArray(Integer.class, o.getAsJsonArray("is_leader_in_dept"))).toArray(new Integer[0]));
        user.setHideMobile(GsonMapper.get(Integer.class, o.get("hide_mobile")));
        user.setEnglishName(GsonMapper.get(String.class, o.get("english_name")));
        user.setTelephone(GsonMapper.get(String.class, o.get("telephone")));
        user.setQrCode(GsonMapper.get(String.class, o.get("qr_code")));
        user.setToInvite(GsonMapper.get(Boolean.class, o.get("to_invite")));
        user.setOpenUserId(GsonMapper.get(String.class, o.get("open_userid")));
        user.setMainDepartment(GsonMapper.get(String.class, o.get("main_department")));
        user.setDirectLeader(Objects.requireNonNull(GsonMapper.getArray(String.class, o.getAsJsonArray("direct_leader"))).toArray(new String[]{}));

        if (o.get(EXTRA_ATTR) != null && !o.get(EXTRA_ATTR).isJsonNull()) {
            this.buildExtraAttrs(o, user);
        }
        if (o.get(EXTERNAL_PROFILE) != null && !o.get(EXTERNAL_PROFILE).isJsonNull()) {

            user.setExternalCorpName(GsonMapper.get(String.class, o.getAsJsonObject().get(EXTERNAL_PROFILE).getAsJsonObject().get(EXTERNAL_CORP_NAME)));
            JsonElement jsonElement = o.get(EXTERNAL_PROFILE).getAsJsonObject().get(WECHAT_CHANNELS);
            if (jsonElement != null) {
                JsonObject asJsonObject = jsonElement.getAsJsonObject();
                user.setWechatChannels(WxCpUser.WechatChannels.builder().nickname(GsonMapper.get(String.class, asJsonObject.get("nickname"))).status(GsonMapper.get(Integer.class, asJsonObject.get("status"))).build());
            }
            this.buildExternalAttrs(o, user);
        }

        user.setExternalPosition(GsonMapper.get(String.class, o.get(EXTERNAL_POSITION)));

        return user;
    }

    private void buildExtraAttrs(JsonObject o, WxCpUser user) {
        JsonArray attrJsonElements = o.get(EXTRA_ATTR).getAsJsonObject().get("attrs").getAsJsonArray();
        for (JsonElement attrJsonElement : attrJsonElements) {
            final Integer type = GsonMapper.get(Integer.class, attrJsonElement.getAsJsonObject().get("type"));
            final WxCpUser.Attr attr = new WxCpUser.Attr().setType(type).setName(GsonMapper.get(String.class, attrJsonElement.getAsJsonObject().get("name")));
            user.getExtAttrs().add(attr);

            if (type == null) {
                attr.setTextValue(GsonMapper.get(String.class, attrJsonElement.getAsJsonObject().get("value")));
                continue;
            }

            switch (type) {
                case 0 -> {
                    attr.setTextValue(GsonMapper.get(String.class, attrJsonElement.getAsJsonObject().get("text").getAsJsonObject().get("value")));
                }
                case 1 -> {
                    final JsonObject web = attrJsonElement.getAsJsonObject().get("web").getAsJsonObject();
                    attr.setWebTitle(GsonMapper.get(String.class, web.get("title"))).setWebUrl(GsonMapper.get(String.class, web.get("url")));
                }
                default -> {
                }//ignored
            }
        }
    }

    private void buildExternalAttrs(JsonObject o, WxCpUser user) {
        JsonElement jsonElement = o.get(EXTERNAL_PROFILE).getAsJsonObject().get(EXTERNAL_ATTR);
        System.err.println(">>>>>\t"+jsonElement);
        if (jsonElement == null) {
            return;
        }

        JsonArray attrJsonElements = jsonElement.getAsJsonArray();
        for (JsonElement element : attrJsonElements) {
            final Integer type = GsonMapper.get(Integer.class, element.getAsJsonObject().get("type"));
            final String name = GsonMapper.get(String.class, element.getAsJsonObject().get("name"));

            if (type == null) {
                continue;
            }

            switch (type) {
                case 0 -> {
                    user.getExternalAttrs().add(WxCpUser.ExternalAttribute.builder().type(type).name(name).value(GsonMapper.get(String.class, element.getAsJsonObject().get("text").getAsJsonObject().get("value"))).build());
                }
                case 1 -> {
                    final JsonObject web = element.getAsJsonObject().get("web").getAsJsonObject();
                    user.getExternalAttrs().add(WxCpUser.ExternalAttribute.builder().type(type).name(name).url(GsonMapper.get(String.class, web.get("url"))).title(GsonMapper.get(String.class, web.get("title"))).build());
                }
                case 2 -> {
                    final JsonObject miniprogram = element.getAsJsonObject().get("miniprogram").getAsJsonObject();
                    user.getExternalAttrs().add(WxCpUser.ExternalAttribute.builder().type(type).name(name).appid(GsonMapper.get(String.class, miniprogram.get("appid"))).pagePath(GsonMapper.get(String.class, miniprogram.get("pagepath"))).title(GsonMapper.get(String.class, miniprogram.get("title"))).build());
                }
                default -> {
                }//ignored
            }
        }
    }

    @Override
    public JsonElement serialize(WxCpUser user, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject o = new JsonObject();
        this.addProperty(o, "userid", user.getUserId());
        this.addProperty(o, "new_userid", user.getNewUserId());
        this.addProperty(o, "name", user.getName());
        if (user.getDepartIds() != null) {
            JsonArray jsonArray = new JsonArray();
            for (Long departId : user.getDepartIds()) {
                jsonArray.add(new JsonPrimitive(departId));
            }
            o.add("department", jsonArray);
        }

        if (user.getOrders() != null) {
            JsonArray jsonArray = new JsonArray();
            for (Integer order : user.getOrders()) {
                jsonArray.add(new JsonPrimitive(order));
            }
            o.add("order", jsonArray);
        }

        this.addProperty(o, "position", user.getPosition());

        if (user.getPositions() != null) {
            JsonArray jsonArray = new JsonArray();
            for (String position : user.getPositions()) {
                jsonArray.add(new JsonPrimitive(position));
            }
            o.add("positions", jsonArray);
        }

        this.addProperty(o, "mobile", user.getMobile());
        if (user.getGender() != null) {
            o.addProperty("gender", user.getGender().getCode());
        }
        this.addProperty(o, "email", user.getEmail());
        this.addProperty(o, "biz_mail", user.getBizMail());
        this.addProperty(o, "avatar", user.getAvatar());
        this.addProperty(o, "thumb_avatar", user.getThumbAvatar());
        this.addProperty(o, "address", user.getAddress());
        this.addProperty(o, "avatar_mediaid", user.getAvatarMediaId());
        this.addProperty(o, "status", user.getStatus());
        this.addProperty(o, "enable", user.getEnable());
        this.addProperty(o, "alias", user.getAlias());
        this.addProperty(o, "isleader", user.getIsLeader());
        if (user.getIsLeaderInDept() != null && user.getIsLeaderInDept().length > 0) {
            JsonArray ary = new JsonArray();
            for (int item : user.getIsLeaderInDept()) {
                ary.add(item);
            }
            o.add("is_leader_in_dept", ary);
        }
        this.addProperty(o, "hide_mobile", user.getHideMobile());
        this.addProperty(o, "english_name", user.getEnglishName());
        this.addProperty(o, "telephone", user.getTelephone());
        this.addProperty(o, "qr_code", user.getQrCode());
        if (user.getToInvite() != null) {
            o.addProperty("to_invite", user.getToInvite());
        }
        this.addProperty(o, "main_department", user.getMainDepartment());

        if (user.getDirectLeader() != null && user.getDirectLeader().length > 0) {
            JsonArray ary = new JsonArray();
            for (String item : user.getDirectLeader()) {
                ary.add(item);
            }
            o.add("direct_leader", ary);
        }
        if (!user.getExtAttrs().isEmpty()) {
            JsonArray attrsJsonArray = new JsonArray();
            for (WxCpUser.Attr attr : user.getExtAttrs()) {
                JsonObject attrJson = GsonMapper.buildJsonObject("type", attr.getType(), "name", attr.getName());
                attrsJsonArray.add(attrJson);

                if (attr.getType() == null) {
                    attrJson.addProperty("name", attr.getName());
                    attrJson.addProperty("value", attr.getTextValue());
                    continue;
                }

                switch (attr.getType()) {
                    case 0:
                        attrJson.add("text", GsonMapper.buildJsonObject("value", attr.getTextValue()));
                        break;
                    case 1:
                        attrJson.add("web", GsonMapper.buildJsonObject("url", attr.getWebUrl(), "title", attr.getWebTitle()));
                        break;
                    default: //ignored
                }
            }
            JsonObject attrsJson = new JsonObject();
            attrsJson.add("attrs", attrsJsonArray);
            o.add(EXTRA_ATTR, attrsJson);
        }

        this.addProperty(o, EXTERNAL_POSITION, user.getExternalPosition());

        JsonObject attrsJson = new JsonObject();
        o.add(EXTERNAL_PROFILE, attrsJson);

        this.addProperty(attrsJson, EXTERNAL_CORP_NAME, user.getExternalCorpName());

        if (user.getWechatChannels() != null) {
            attrsJson.add(WECHAT_CHANNELS, GsonMapper.buildJsonObject("nickname", user.getWechatChannels().getNickname(), "status", user.getWechatChannels().getStatus()));
        }

        if (!user.getExternalAttrs().isEmpty()) {
            JsonArray attrsJsonArray = new JsonArray();
            for (WxCpUser.ExternalAttribute attr : user.getExternalAttrs()) {
                JsonObject attrJson = GsonMapper.buildJsonObject("type", attr.getType(), "name", attr.getName());

                attrsJsonArray.add(attrJson);

                if (attr.getType() == null) {
                    continue;
                }

                switch (attr.getType()) {
                    case 0:
                        attrJson.add("text", GsonMapper.buildJsonObject("value", attr.getValue()));
                        break;
                    case 1:
                        attrJson.add("web", GsonMapper.buildJsonObject("url", attr.getUrl(), "title", attr.getTitle()));
                        break;
                    case 2:
                        attrJson.add("miniprogram", GsonMapper.buildJsonObject("appid", attr.getAppid(), "pagepath", attr.getPagePath(), "title", attr.getTitle()));
                        break;
                    default://忽略
                }
            }

            attrsJson.add(EXTERNAL_ATTR, attrsJsonArray);
        }

        return o;
    }

    private void addProperty(JsonObject object, String property, Integer value) {
        if (value != null) {
            object.addProperty(property, value);
        }
    }

    private void addProperty(JsonObject object, String property, String value) {
        if (value != null) {
            object.addProperty(property, value);
        }
    }

}