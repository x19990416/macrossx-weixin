package com.xyuxin.weixin.cp.bean.pay;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 商品信息详情
 */
@Data
public class WxCpRefund {
    /**
     * 退款单号
     */
    private String outRefundNo;
    /**
     * 退款发起人ID
     */
    private String refundUserid;
    /**
     * 退款备注
     */
    private String refundComment;
    /**
     * 退款发起时间
     */
    private Long refundReqtime;
    /**
     * 退款状态：
     * 0：已申请退款
     * 1：退款处理中
     * 2：退款成功
     * 3：退款关闭
     * 4：退款异常
     * 5：审批中
     * 6：审批失败
     * 7：审批取消
     */
    private Integer refundStatus;
    /**
     * 退款金额
     */
    private BigDecimal refundFee;
}
