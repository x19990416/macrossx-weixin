package com.xyuxin.weixin.cp.api.impl;

import com.google.gson.Gson;
import com.xyuxin.weixin.common.bean.WxAccessToken;
import com.xyuxin.weixin.common.utils.http.BaseHttpGetExecutor;
import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.cp.WxCpConfig;
import com.xyuxin.weixin.cp.api.WxCpService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * 微信企业号基础实现类
 *
 * @author 50014792@qq.com
 * @date 2023/10/27
 */
@Slf4j
public class BaseWxCpServiceImpl implements WxCpService {
    @Getter
    private final WxCpConfig config;
    private static WxAccessToken wxAccessToken;
    @Getter
    private final HttpClientExecutor executorFactory;


    public BaseWxCpServiceImpl(WxCpConfig config, HttpClientExecutor executorFactory) {
        this.config = config;
        this.executorFactory = executorFactory;
    }

    protected synchronized WxAccessToken getWxAccessToken() throws IOException {
        String url = String.format(WxCpService.GET_ACCESS_TOKEN_URL, config.getCorpid(), config.getCorpsecret());
        log.info("GET_ACCESS_TOKEN_URL: {}", url);
        BaseHttpGetExecutor<String> executor = executorFactory.httpGetStringExecutor();
        WxAccessToken result = new Gson().fromJson(executor.execute(url, null), WxAccessToken.class);
        log.info("{}", result);
        return result;
    }

    @Override
    public String getAccessToken() throws IOException {
        if (wxAccessToken == null || wxAccessToken.isOutExpire()) {
            wxAccessToken = getWxAccessToken();
        }
        return wxAccessToken.getAccessToken();
    }
}
