package com.xyuxin.weixin.cp.api;

import com.xyuxin.weixin.common.service.WxService;
import com.xyuxin.weixin.cp.bean.miniapppay.BillDownloadInfo;
import com.xyuxin.weixin.cp.bean.miniapppay.OrderPayDetail;

import javax.validation.constraints.NotEmpty;
import java.io.IOException;
import java.util.List;

public interface WxCpMiniAppPayService extends WxService {
    String POST_GET_BILL="https://qyapi.weixin.qq.com/cgi-bin/miniapppay/get_bill?access_token=%s";
            //&bill_date=%s&mchid=%s&bill_type=%s&tar_type=%s";

    BillDownloadInfo getBill(@NotEmpty String billDate,@NotEmpty String mchid, String billType, String tarType) throws IOException;

    String GET_BILL_DOWNLOAD_FILE="https://api.mch.weixin.qq.com/v3/billdownload/file?token=%s";

    List<String> billDownloadFile(String url, String auth) throws IOException, InterruptedException;

    String POST_GET_ORDER = "https://qyapi.weixin.qq.com/cgi-bin/miniapppay/get_order?access_token=%s";

    OrderPayDetail getOrder(String mcid, String outTradeNo) throws IOException;



}
