package com.xyuxin.weixin.cp.api;

import com.xyuxin.weixin.cp.bean.menu.WxCpMenuInfo;

import java.io.IOException;

public interface WxCpMenuService {

    String POST_MENU_CREATE = "https://qyapi.weixin.qq.com/cgi-bin/menu/create?access_token=%s&agentid=%s";

    boolean create(WxCpMenuInfo menuInfo) throws IOException;

    String GET_MENU_GET = "https://qyapi.weixin.qq.com/cgi-bin/menu/get?access_token=%s&agentid=%s";

    WxCpMenuInfo get() throws IOException;

    String GET_MENU_DELETE = "https://qyapi.weixin.qq.com/cgi-bin/menu/delete?access_token=%s&agentid=%s";

    boolean delete() throws IOException;
}
