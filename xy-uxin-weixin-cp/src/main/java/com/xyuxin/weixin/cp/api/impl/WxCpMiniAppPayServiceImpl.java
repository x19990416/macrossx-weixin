package com.xyuxin.weixin.cp.api.impl;

import com.google.gson.Gson;
import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.cp.WxCpConfig;
import com.xyuxin.weixin.cp.api.WxCpMiniAppPayService;
import com.xyuxin.weixin.cp.bean.miniapppay.BillDownloadInfo;
import com.xyuxin.weixin.cp.bean.miniapppay.OrderPayDetail;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class WxCpMiniAppPayServiceImpl extends BaseWxCpServiceImpl implements WxCpMiniAppPayService {
    public WxCpMiniAppPayServiceImpl(WxCpConfig config, HttpClientExecutor executorFactory) {
        super(config, executorFactory);
    }

    @Override
    public BillDownloadInfo getBill(String billDate, String mchid, String billType, String tarType) throws IOException {
        String url = String.format(WxCpMiniAppPayService.POST_GET_BILL, super.getAccessToken());

        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        Map<String, Object> data = new HashMap<>();
        data.put("bill_date", billDate);
        data.put("mchid", mchid);
        data.put("bill_type", billType);
        data.put("tar_type", tarType);

        String response = executor.execute(url, new Gson().toJson(data));
        return new Gson().fromJson(response, BillDownloadInfo.class);
    }

    public List<String> billDownloadFile(String downloadUrl, String auth) throws IOException, InterruptedException {
        URL url = new URL(downloadUrl);
        String[] headerParams = auth.split(":");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            connection.setRequestMethod("GET");
            connection.setRequestProperty(headerParams[0], headerParams[1]);
            connection.connect();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                List<String> lines = new ArrayList<>();
                String line;
                while ((line = reader.readLine()) != null) {
                    lines.add(line);
                }
                return lines;
            }
        } finally {
            connection.disconnect();
        }
    }

    @Override
    public OrderPayDetail getOrder(String mchid, String outTradeNo) throws IOException {
        String url = String.format(WxCpMiniAppPayService.POST_GET_ORDER, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        String response = executor.execute(url, new Gson().toJson(Map.of("mchid",mchid,"out_trade_no",outTradeNo)));
        return new Gson().fromJson(response, OrderPayDetail.class);
    }
}
