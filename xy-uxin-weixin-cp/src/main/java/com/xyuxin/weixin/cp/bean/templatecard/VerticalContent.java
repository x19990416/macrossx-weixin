package com.xyuxin.weixin.cp.bean.templatecard;

import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VerticalContent  {

    /**
     * 卡片二级标题，建议不超过38个字.必填字段
     */
    private String title;
    /**
     * 二级普通文本，建议不超过160个字
     */
    private String desc;

    /**
     * To json json object.
     *
     * @return the json object
     */
    public JsonObject toJson() {
        JsonObject vContentJson = new JsonObject();

        vContentJson.addProperty("title", this.getTitle());

        if (StringUtils.isNotBlank(this.getDesc())) {
            vContentJson.addProperty("desc", this.getDesc());
        }
        return vContentJson;
    }
}
