package com.xyuxin.weixin.cp.bean.miniapppay;

import com.google.gson.annotations.SerializedName;
import com.xyuxin.weixin.common.bean.WxErr;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class OrderPayDetail extends WxErr {
    private String mcid;
    @SerializedName("out_trade_no")
    private String outTradeNo;
    @SerializedName("trade_state")
    private String tradeState;
    @SerializedName("trade_state_desc")
    private String tradeStateDesc;
    private Payer payer;
    @SerializedName("transaction_id")
    private String transactionId;

    @Data
    public static class Payer {
        private String openid;
    }

}
