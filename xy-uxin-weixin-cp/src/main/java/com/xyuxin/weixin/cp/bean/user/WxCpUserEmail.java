package com.xyuxin.weixin.cp.bean.user;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * 用户邮箱
 *
 * @see <a href="https://developer.work.weixin.qq.com/document/path/95895">邮箱获取userid</a>
 */
@Data
public class WxCpUserEmail {
    /**
     * 邮箱
     */

    private String email;
    /**
     * 邮箱类型：1-企业邮箱（默认）；2-个人邮箱
     */
    @SerializedName("email_type")
    private String emailType;
}
