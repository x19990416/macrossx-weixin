package com.xyuxin.weixin.cp.bean.pay;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class WxCpBill {
    private String transactionId;
    private Integer billType;
    private Integer tradeState;
    private Long payTime;
    private String outTradeNo;
    private String outRefundNo;
    private String externalUserid;
    private BigDecimal totalFee;
    private String payeeUserid;
    private Integer paymentType;
    private String mchId;
    private String remark;
    private List<WxCpCommodity> commodityList;
    private BigDecimal totalRefundFee;
    private List<WxCpRefund> refundList;
    private WxCpContact contactInfo;
    private WxCpMiniProgram miniprogramInfo;



}
