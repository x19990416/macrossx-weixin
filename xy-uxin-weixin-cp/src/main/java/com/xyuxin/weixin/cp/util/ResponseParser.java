package com.xyuxin.weixin.cp.util;

import com.xyuxin.weixin.common.utils.AESUtil;
import com.xyuxin.weixin.common.utils.SignUtil;
import com.xyuxin.weixin.cp.bean.WxCpXmlMessage;
import org.apache.commons.lang3.RandomStringUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Base64;

public class ResponseParser {

    public static byte[] toEncodeParser(String data, String receiveId) throws IOException {
        SecureRandom random = new SecureRandom();
        byte[] randomBytes = new byte[16];
        random.nextBytes(randomBytes);
        byte[] msgLen = ByteBuffer.allocate(4).putInt(data.getBytes(StandardCharsets.UTF_8).length).array();
        byte[] msgData = data.getBytes(StandardCharsets.UTF_8);
        byte[] sReceiveId = receiveId.getBytes(StandardCharsets.UTF_8);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        output.write(randomBytes);
        output.write(msgLen);
        output.write(msgData);
        output.write(sReceiveId);
        return output.toByteArray();
    }

    public static WxCpXmlMessage parse(WxCpXmlMessage result, String receiveId, String aesKey, String token) throws IOException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException, NoSuchProviderException {
        WxCpXmlMessage ret = new WxCpXmlMessage();
        ret.setTimestamp(System.currentTimeMillis());
        ret.setNonce(RandomStringUtils.randomAlphanumeric(8));
        String data = XStreamTransformer.toXml(WxCpXmlMessage.class, result);
        String encodedData = Base64.getEncoder().encodeToString(AESUtil.encrypt(toEncodeParser(data, receiveId), AESUtil.loadSecretKey(aesKey)));
        ret.setEncrypt(encodedData);

        String sign = SignUtil.sign(ret.getNonce(), token, ret.getEncrypt(), String.valueOf(ret.getTimestamp()));
        ret.setMsgSignature(sign);
        return ret;
    }

}
