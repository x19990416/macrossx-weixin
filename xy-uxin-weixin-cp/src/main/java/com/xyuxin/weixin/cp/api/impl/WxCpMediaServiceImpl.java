package com.xyuxin.weixin.cp.api.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.xyuxin.weixin.common.bean.WxErr;
import com.xyuxin.weixin.common.utils.http.BaseHttpFileExecutor;
import com.xyuxin.weixin.common.utils.http.BaseHttpGetExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.common.utils.http.apache.ApacheHttpClientExecutor;
import com.xyuxin.weixin.common.utils.http.bean.BaseFileEntity;
import com.xyuxin.weixin.cp.WxCpConfig;
import com.xyuxin.weixin.cp.WxCpConstants;
import com.xyuxin.weixin.cp.api.WxCpMediaService;
import com.xyuxin.weixin.cp.bean.media.WxMediaUploadResult;
import com.xyuxin.weixin.cp.bean.message.WxCpMessage;
import com.xyuxin.weixin.cp.bean.user.WxCpUserEmail;

import java.io.*;
import java.util.Map;

public class WxCpMediaServiceImpl extends BaseWxCpServiceImpl implements WxCpMediaService {
    public WxCpMediaServiceImpl(WxCpConfig config, HttpClientExecutor executorFactory) {
        super(config, executorFactory);
    }

    @Override
    public WxMediaUploadResult upload(BaseFileEntity entity,String type) throws IOException {
        String url = String.format(WxCpMediaService.POST_MEDIA_UPLOAD, super.getAccessToken(), type);
        url = url+"&debug=1";
        System.out.println(url);
        BaseHttpFileExecutor<String> executor = super.getExecutorFactory().httpPostFileExecutor();
        String result = executor.execute(url, Map.of("media", entity));
        System.out.println(result);
        return new Gson().fromJson(result, WxMediaUploadResult.class);
    }

    @Override
    public String uploadImg(File imgFile) throws IOException {
        String url = String.format(WxCpMediaService.POST_MEDIA_UPLOAD_IMG, super.getAccessToken());
        BaseHttpFileExecutor<String> executor = super.getExecutorFactory().httpPostFileExecutor();
        String content = executor.execute(url, Map.of("file", imgFile));
        JsonObject jsonObj = JsonParser.parseString(content).getAsJsonObject();
        if (jsonObj.get("errcode").getAsInt() == 0) {
            return jsonObj.get("url").getAsString();
        }
        throw new RuntimeException(content);
    }

    @Override
    public InputStream getStream(String mediaId) throws IOException {
        String url = String.format(WxCpMediaService.GET_MEDIA, super.getAccessToken(),mediaId);
        BaseHttpFileExecutor<InputStream> executor = super.getExecutorFactory().httpGetFileExecutor();
        InputStream stream = executor.execute(url,null);
        return stream;
    }
    @Override
    public String getUrl(String mediaId) throws IOException {
        String url = String.format(WxCpMediaService.GET_MEDIA, super.getAccessToken(),mediaId);
        return url;
    }

    @Override
    public String getJssdk(String mediaId) throws IOException {
        String url = String.format(WxCpMediaService.GET_MEDIA_JSSDK, super.getAccessToken(),mediaId);
        BaseHttpGetExecutor<String> executor = super.getExecutorFactory().httpGetStringExecutor();
        String content = executor.execute(url,null);
        JsonObject jsonObj = JsonParser.parseString(content).getAsJsonObject();
        if (jsonObj.get("errcode")!=null) {
            throw new RuntimeException(content);
        }else{
            return url;
        }
    }

    public static void main(String...s) throws IOException {
        String secret ="SAPzk1kuAO7bZ040N0LJA_807hWhrvHeBkVyOxInFXA";
        String corpid = "ww21eb70ce0daefe83";
        String secret2 = "_mq9CrAKgC5HMH_hKolLL2NLJuia3oSb4OlROcBZS00";
        WxCpConfig config = new WxCpConfig().setCorpsecret(secret2).setCorpid(corpid);
        WxCpUserServiceImpl userService1 = new WxCpUserServiceImpl(config,new ApacheHttpClientExecutor());
        WxCpUserEmail email = new WxCpUserEmail();
        email.setEmail("zoey@colourfuldawn.com");
        email.setEmailType("1");
        System.out.println(userService1.getUserIdByEmail(email));
        System.exit(-1);
        WxCpTemplateMessageServiceImpl userService = new WxCpTemplateMessageServiceImpl(config,new ApacheHttpClientExecutor());
        WxCpMediaServiceImpl mediaService = new WxCpMediaServiceImpl(config,new ApacheHttpClientExecutor());

        BaseFileEntity entity = new BaseFileEntity();
        entity.setFileName("log.log");
        entity.setFileStream(new FileInputStream("d://test.dat"));
        //System.out.println(mediaService.upload(entity,"file"));
        // 3nlY17ZXYUZfCnPS2oAs5q_-YwJvOtkWctCKIHoc-Q6Tk3mF8NjF3CZY-1TGkJvo4
        WxCpMessage msg = new WxCpMessage();
        msg.setToUser("XiYangYangDeChengZi|x");
        msg.setMsgType("file");
        msg.setAgentId(1000002);
        msg.setContent("hi1");
        msg.setEnableIdTrans(false);
        msg.setEnableDuplicateCheck(true);
        msg.setDuplicateCheckInterval(1800);
        msg.setMediaId("3nlY17ZXYUZfCnPS2oAs5q_-YwJvOtkWctCKIHoc-Q6Tk3mF8NjF3CZY-1TGkJvo4");
        userService.send(msg);
    }

}
