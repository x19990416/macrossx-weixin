package com.xyuxin.weixin.cp.api;

import com.xyuxin.weixin.cp.WxCpConstants;
import com.xyuxin.weixin.cp.bean.user.*;

import java.io.IOException;

/**
 * 成员管理
 *
 * @author guolimin
 * @see <a href="https://developer.work.weixin.qq.com/document/path/90329">成员管理</a>
 */
public interface WxCpUserService {
    String POST_USER_LIST = "https://qyapi.weixin.qq.com/cgi-bin/user/list_id?access_token=%s";

    /**
     * 获取成员ID列表
     *
     * @see <a href="https://developer.work.weixin.qq.com/document/path/96067">获取成员ID列表</a>
     */
    WxCpDeptUserResult getUserListId(String cursor, Integer limit) throws IOException;

    String POST_USER_CREATE = "https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token=%s";

    /**
     * 创建成员
     *
     * @see <a href="https://developer.work.weixin.qq.com/document/path/90195">创建成员</a>
     */
    boolean create(WxCpUser user) throws IOException;

    String GET_USER_GET = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=%s&userid=%s";

    /**
     * 读取成员
     *
     * @see <a href="https://developer.work.weixin.qq.com/document/path/90196">读取成员</a>
     */
    WxCpUser getByUserId(String userId) throws IOException;

    String POST_USER_UPDATE = "https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token=%s";

    /**
     * 更新成员
     *
     * @see <a href="https://developer.work.weixin.qq.com/document/path/90197">更新成员</a>
     */
    boolean update(WxCpUser user) throws IOException;

    String GET_USER_DELETE = "https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token=%s&userid=%s";

    /**
     * 删除成员
     *
     * @see <a href="https://developer.work.weixin.qq.com/document/path/90198">删除成员</a>
     */
    boolean delete(String userId) throws IOException;

    String POST_USER_DELETE_BATCH = "https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token=%s";

    /**
     * 批量删除成员
     *
     * @see <a href="https://developer.work.weixin.qq.com/document/path/90199">批量删除成员</a>
     */
    boolean deleteBatch(String... batchIds) throws IOException;

    String POST_CONVERT_TO_OPENID = "https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_openid?access_token=%s";

    /**
     * userid与openid互换
     *
     * @see <a href="https://developer.work.weixin.qq.com/document/path/90202">userid与openid互换</a>>
     */
    String convertToOpenid(String userId) throws IOException;

    String GET_USER_AUTH_SUCC = "https://qyapi.weixin.qq.com/cgi-bin/user/authsucc?access_token=%s&userid=%s";

    /**
     * 二次验证
     */
    boolean authSucc(String userId) throws IOException;

    String POST_USER_BATCH_INVITE = "https://qyapi.weixin.qq.com/cgi-bin/batch/invite?access_token=%s";

    /**
     * 邀请成员
     *
     * @see <a href="https://developer.work.weixin.qq.com/document/path/90975">邀请成员</a>
     */
    WxCpUserInvalidInvite batchInvite(WxCpUserInvite invite) throws IOException;


    String GET_JOIN_QRCODE = "https://qyapi.weixin.qq.com/cgi-bin/corp/get_join_qrcode?access_token=%s&size_type=%s";

    /**
     * 获取加入企业二维码
     *
     * @see <a href="https://developer.work.weixin.qq.com/document/path/91714">获取加入企业二维码</a>
     */
    String getJoinQrcode(WxCpConstants.QrCodeSize size) throws IOException;

    String POST_GET_USERID_BY_MOBILE = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserid?access_token=%s";

    /**
     * 手机号获取userid
     *
     * @see <a href="https://developer.work.weixin.qq.com/document/path/95402">手机号获取userid</a>
     */
    String getUserIdByMobile(String mobile) throws IOException;

    String POST_GET_USERID_BY_EMAIL = "https://qyapi.weixin.qq.com/cgi-bin/user/get_userid_by_email?access_token=%s";

    /**
     * 邮箱获取userid
     *
     * @see <a href="https://developer.work.weixin.qq.com/document/path/95895">邮箱获取userid</a>
     */
    String getUserIdByEmail(WxCpUserEmail email) throws IOException;

}
