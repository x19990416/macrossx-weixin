package com.xyuxin.weixin.cp.api.impl;

import com.xyuxin.weixin.common.utils.http.BaseHttpGetExecutor;
import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.common.utils.http.apache.ApacheHttpClientExecutor;
import com.xyuxin.weixin.cp.WxCpConfig;
import com.xyuxin.weixin.cp.WxCpConstants;
import com.xyuxin.weixin.cp.api.WxCpWebHookService;
import com.xyuxin.weixin.cp.bean.article.NewArticle;
import com.xyuxin.weixin.cp.bean.message.WxCpWebHookMessage;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class WxCpWebHookServiceImpl extends BaseWxCpServiceImpl implements WxCpWebHookService {
    public WxCpWebHookServiceImpl(WxCpConfig config, HttpClientExecutor executorFactory) {
        super(config, executorFactory);
    }

    @Override
    public void sendText(String content, List<String> mentionedList, List<String> mobileList) throws IOException {

    }

    @Override
    public void sendMarkdown(String content) throws IOException {

    }

    @Override
    public void sendImage(String base64, String md5) throws IOException {

    }

    @Override
    public void sendNews(List<NewArticle> articleList) throws IOException {

    }

    @Override
    public void sendText(String webhookUrl, String content, List<String> mentionedList, List<String> mobileList) throws IOException {
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        WxCpWebHookMessage message = new WxCpWebHookMessage();
        message.setMsgType(WxCpConstants.WebHookMsgType.TEXT);
        message.setContent("hello 江老板好");
        message.setMentionedList(mentionedList);
        message.setMentionedMobileList(mobileList);
        System.out.println(executor.execute(webhookUrl, message.toJson()));

    }

    @Override
    public void sendMarkdown(String webhookUrl, String content) throws IOException {

    }

    @Override
    public void sendImage(String webhookUrl, String base64, String md5) throws IOException {

    }

    @Override
    public void sendNews(String webhookUrl, List<NewArticle> articleList) throws IOException {

    }

    @Override
    public void sendFile(String webhookUrl, String mediaId) throws IOException {

    }

    @Override
    public void sendVoice(String webhookUrl, String mediaId) throws IOException {

    }

    @Override
    public void sendTemplateCardMessage(String webhookUrl, WxCpWebHookMessage wxCpWebHookMessage) throws IOException {

    }

    public static void main(String...s) throws IOException {
        WxCpWebHookServiceImpl impl =new WxCpWebHookServiceImpl(new WxCpConfig(),new ApacheHttpClientExecutor());
        impl.sendText("https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=8dc37395-1de6-4e04-8f23-e7441d14030e",
                "test", Arrays.asList("x","ZoeyJiang"),null);
    }
}
