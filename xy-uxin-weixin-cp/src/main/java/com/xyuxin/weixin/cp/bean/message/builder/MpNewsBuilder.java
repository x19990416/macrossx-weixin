package com.xyuxin.weixin.cp.bean.message.builder;

import com.xyuxin.weixin.cp.WxCpConstants;
import com.xyuxin.weixin.cp.bean.article.MpNewsArticle;
import com.xyuxin.weixin.cp.bean.message.WxCpMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * mpnews类型的图文消息builder
 * <pre>
 * 用法:
 * WxCustomMessage m = WxCustomMessage.MPNEWS().addArticle(article).toUser(...).build();
 * </pre>
 *
 * @author Binary Wang
 */
public final class MpNewsBuilder extends BaseBuilder<MpNewsBuilder> {
    private List<MpNewsArticle> articles = new ArrayList<>();

    private String mediaId;

    /**
     * Instantiates a new Mpnews builder.
     */
    public MpNewsBuilder() {
        this.msgType = WxCpConstants.KefuMsgType.MPNEWS;
    }

    /**
     * Media id mpnews builder.
     *
     * @param mediaId the media id
     * @return the mpnews builder
     */
    public MpNewsBuilder mediaId(String mediaId) {
        this.mediaId = mediaId;
        return this;
    }

    /**
     * Add article mpnews builder.
     *
     * @param articles the articles
     * @return the mpnews builder
     */
    public MpNewsBuilder addArticle(MpNewsArticle... articles) {
        Collections.addAll(this.articles, articles);
        return this;
    }

    /**
     * Articles mpnews builder.
     *
     * @param articles the articles
     * @return the mpnews builder
     */
    public MpNewsBuilder articles(List<MpNewsArticle> articles) {
        this.articles = articles;
        return this;
    }

    @Override
    public WxCpMessage build() {
        WxCpMessage m = super.build();
        m.setMpNewsArticles(this.articles);
        if (this.mediaId != null) {
            m.setMediaId(this.mediaId);
        }

        return m;
    }
}
