package com.xyuxin.weixin.cp.api;

import com.xyuxin.weixin.cp.bean.external.contact.GroupChatGet;
import com.xyuxin.weixin.cp.bean.external.contact.GroupChatGetReq;
import com.xyuxin.weixin.cp.bean.external.contact.GroupChatListReq;
import com.xyuxin.weixin.cp.bean.external.contact.GroupChatList;
import lombok.Getter;

import java.io.IOException;

public interface WxCpExternalContactService {
    String POST_GROUPCHAT_LIST = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/groupchat/list?access_token=%s";

    /**
     * 获取客户群列表
     */
    GroupChatList groupChatList(GroupChatListReq request) throws IOException;

    /**
     * 获取客户群详情
     * @see <a href="https://developer.work.weixin.qq.com/document/path/96338">获取客户群详情</a>
     */
    String POST_GROUPCHAT_GET = "https://qyapi.weixin.qq.com/cgi-bin/externalcontact/groupchat/get?access_token=%s";

    GroupChatGet groupChatGet(GroupChatGetReq request) throws IOException;


    enum GroupChatStatusFilter {
        ALL("所有列表(即不过滤)", 0),
        /**
         * 男
         */
        PENDING_INHERITANCE("离职待继承", 1),
        /**
         * 女
         */
        INHERITING("离职继承中", 2), INHERITANCE_COMPLETED("离职继承完成", 3);
        @Getter
        private final String text;
        @Getter
        private final Integer code;

        GroupChatStatusFilter(String text, Integer code) {
            this.text = text;
            this.code = code;
        }

    }
    enum GroupChatListStatus {
        NORMAL("跟进人正常", 0),
        RESIGNED("跟进人离职", 1),
        INHERITING("离职继承中", 2),
        INHERITANCE_COMPLETED("离职继承完成", 3);
        @Getter
        private final String text;
        @Getter
        private final Integer code;

        GroupChatListStatus(String text, Integer code) {
            this.text = text;
            this.code = code;
        }

    }

}
