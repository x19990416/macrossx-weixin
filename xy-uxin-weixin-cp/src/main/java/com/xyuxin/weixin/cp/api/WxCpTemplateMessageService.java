package com.xyuxin.weixin.cp.api;

import com.xyuxin.weixin.cp.bean.message.WxCpMessage;
import com.xyuxin.weixin.cp.bean.message.WxCpMessageSendResult;

import java.io.IOException;

/**
 * 发送应用消息
 * @see <a href="https://developer.work.weixin.qq.com/document/path/90372">发送应用消息</a>
 */
public interface WxCpTemplateMessageService {
    String POST_MESSAGE_SEND = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=%s";

    WxCpMessageSendResult send(WxCpMessage message) throws IOException;


}
