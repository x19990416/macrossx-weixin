package com.xyuxin.weixin.cp.api.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.xyuxin.weixin.common.bean.WxErr;
import com.xyuxin.weixin.common.utils.gson.GsonMapper;
import com.xyuxin.weixin.common.utils.http.BaseHttpGetExecutor;
import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.cp.WxCpConfig;
import com.xyuxin.weixin.cp.WxCpConstants;
import com.xyuxin.weixin.cp.api.WxCpUserService;
import com.xyuxin.weixin.cp.bean.user.*;
import com.xyuxin.weixin.cp.util.gson.GsonInitializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Map;

/**
 * @author guolimin
 */
@Slf4j
public class WxCpUserServiceImpl extends BaseWxCpServiceImpl implements WxCpUserService {
    public WxCpUserServiceImpl(WxCpConfig config, HttpClientExecutor executorFactory) {
        super(config, executorFactory);
    }

    @Override
    public WxCpDeptUserResult getUserListId(String cursor, Integer limit) throws IOException {
        String url = String.format(WxCpUserService.POST_USER_LIST, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        Map<String, Object> src = new java.util.HashMap<>();
        src.put("cursor", cursor);
        src.put("limit", limit);
        return new Gson().fromJson(executor.execute(url, new Gson().toJson(src)), WxCpDeptUserResult.class);
    }

    public boolean create(WxCpUser user) throws IOException {
        String url = String.format(WxCpUserService.POST_USER_CREATE, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        String response = executor.execute(url, new Gson().toJson(user));
        WxErr resp = new Gson().fromJson(response, WxErr.class);
        if (1 == resp.getErrCode()) {
            return true;
        } else {
            log.info("create user error: [{}]", resp);
            return false;
        }


    }

    @Override
    public WxCpUser getByUserId(String userId) throws IOException {
        String url = String.format(WxCpUserService.GET_USER_GET, super.getAccessToken(), userId);
        BaseHttpGetExecutor<String> executor = super.getExecutorFactory().httpGetStringExecutor();
        return GsonInitializer.getInstance().fromJson(executor.execute(url, null), WxCpUser.class);
    }

    @Override
    public boolean update(WxCpUser user) throws IOException {
        String url = String.format(WxCpUserService.POST_USER_UPDATE, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        WxErr resp = new Gson().fromJson(executor.execute(url, GsonInitializer.getInstance().toJson(user)), WxErr.class);
        if (1 != resp.getErrCode()) {
            log.error("update user error: [{}]", resp);
        }
        return 1 == resp.getErrCode();
    }

    @Override
    public boolean delete(String userId) throws IOException {
        String url = String.format(WxCpUserService.GET_USER_DELETE, super.getAccessToken(), userId);
        BaseHttpGetExecutor<String> executor = super.getExecutorFactory().httpGetStringExecutor();
        WxErr resp = new Gson().fromJson(executor.execute(url, null), WxErr.class);
        if (1 != resp.getErrCode()) {
            log.error("delete user error: [{}]", resp);
        }
        return 1 == resp.getErrCode();
    }

    @Override
    public boolean deleteBatch(String... batchIds) throws IOException {
        String url = String.format(WxCpUserService.POST_USER_DELETE_BATCH, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        JsonObject request = new JsonObject();
        request.add("useridlist", new Gson().toJsonTree(batchIds));
        WxErr resp = new Gson().fromJson(executor.execute(url, request.toString()), WxErr.class);
        if (1 != resp.getErrCode()) {
            log.error("delete user error: [{}]", resp);
        }
        return 1 == resp.getErrCode();
    }

    @Override
    public String convertToOpenid(String userId) throws IOException {
        String url = String.format(WxCpUserService.POST_CONVERT_TO_OPENID, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        String content = executor.execute(url, GsonMapper.buildJsonObject("userid", userId).toString());
        JsonObject jsonObj = JsonParser.parseString(content).getAsJsonObject();
        if (jsonObj.get("errcode").getAsInt() == 0) {
            return jsonObj.get("openid").getAsString();
        }
        throw new RuntimeException(content);
    }

    @Override
    public boolean authSucc(String userId) throws IOException {
        String url = String.format(WxCpUserService.GET_USER_AUTH_SUCC, super.getAccessToken(), userId);
        BaseHttpGetExecutor<String> executor = super.getExecutorFactory().httpGetStringExecutor();
        String content = executor.execute(url, null);
        JsonObject jsonObj = JsonParser.parseString(content).getAsJsonObject();
        return jsonObj.get("errcode").getAsInt() == 0;
    }

    @Override
    public WxCpUserInvalidInvite batchInvite(WxCpUserInvite invite) throws IOException {
        String url = String.format(WxCpUserService.POST_USER_BATCH_INVITE, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        return new Gson().fromJson(executor.execute(url, new Gson().toJson(invite)), WxCpUserInvalidInvite.class);
    }

    @Override
    public String getJoinQrcode(WxCpConstants.QrCodeSize codeSize) throws IOException {
        String url = String.format(WxCpUserService.GET_JOIN_QRCODE, super.getAccessToken(), codeSize.size());
        BaseHttpGetExecutor<String> executor = super.getExecutorFactory().httpGetStringExecutor();
        String content = executor.execute(url, null);
        JsonObject jsonObj = JsonParser.parseString(content).getAsJsonObject();
        if (jsonObj.get("errcode").getAsInt() == 0) {
            return jsonObj.get("join_qrcode").getAsString();
        }
        throw new RuntimeException(content);
    }

    @Override
    public String getUserIdByMobile(String mobile) throws IOException {
        String url = String.format(WxCpUserService.POST_GET_USERID_BY_MOBILE, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        String content = executor.execute(url, GsonMapper.buildJsonObject("mobile", mobile).toString());
        JsonObject jsonObj = JsonParser.parseString(content).getAsJsonObject();
        if (jsonObj.get("errcode").getAsInt() == 0) {
            return jsonObj.get("userid").getAsString();
        }
        throw new RuntimeException(content);
    }

    @Override
    public String getUserIdByEmail(WxCpUserEmail email) throws IOException {
        String url = String.format(WxCpUserService.POST_GET_USERID_BY_EMAIL, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        String content = executor.execute(url, new Gson().toJson(email));
        JsonObject jsonObj = JsonParser.parseString(content).getAsJsonObject();
        if (jsonObj.get("errcode").getAsInt() == 0) {
            return jsonObj.get("userid").getAsString();
        }
        throw new RuntimeException(content);
    }
}
