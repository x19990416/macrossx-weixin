package com.xyuxin.weixin.cp.bean.taskcard;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskCardButton  {

    private String key;
    private String name;
    private String replaceName;
    private String color;
    private Boolean bold;
}