package com.xyuxin.weixin.cp.api;

import com.xyuxin.weixin.cp.WxCpConstants;
import com.xyuxin.weixin.cp.bean.WxCpXmlMessage;
import com.xyuxin.weixin.cp.util.ResponseParser;

import javax.validation.constraints.NotNull;

/**
 * 接收消息与事件
 *
 * @see <a href="https://developer.work.weixin.qq.com/document/path/90375">接收消息与事件</a>
 */

public interface WxCpBaseMessageService {

    default WxCpXmlMessage process(@NotNull WxCpXmlMessage msg) {
        WxCpXmlMessage result = switch (WxCpConstants.BasicMsgType.valueOf(msg.getMsgType().toUpperCase())) {
            case TEXT -> processTextMsg(msg);
            case IMAGE -> processImageMsg(msg);
            case LINK -> processLinkMsg(msg);
            case LOCATION -> processLocationMsg(msg);
            case SHORT_VIDEO -> processShortVideoMsg(msg);
            case VIDEO -> processVideoMsg(msg);
            case VOICE -> processVoiceMsg(msg);
            case EVENT -> processEvent(msg);
            default ->
                    throw new IllegalStateException("Unexpected value: " + WxCpConstants.BasicMsgType.valueOf(msg.getMsgType().toUpperCase()));
        };
        if (result == null) {
            result = new WxCpXmlMessage();
            result.setMsgType("text");
            result.setContent("");
        }

        result.setToUserName(msg.getFromUserName());
        result.setFromUserName(msg.getToUserName());
        result.setCreateTime(System.currentTimeMillis());

        return result;
    }

    /**
     * 事件消息处理入口
     *
     * @param msg
     * @return
     */
    default WxCpXmlMessage processEvent(WxCpXmlMessage msg) {
        return switch (WxCpConstants.BasicEventMsgType.valueOf(msg.getEvent().toUpperCase())) {
            case SUBSCRIBE -> processSubscribeEvent(msg);
            case UNSUBSCRIBE -> processUnsubscribeEvent(msg);
            case SCAN -> processScanEvent(msg);
            case LOCATION -> processLocationEvent(msg);
            case CLICK -> processClickEvent(msg);
            case VIEW -> processViewEvent(msg);
            case TEMPLATESENDJOBFINISH -> processTemplateSendJobFinish(msg);
        };

    }

    /**
     * 点击菜单跳转链接时的事件推送
     *
     * @param msg
     * @return
     */
    default WxCpXmlMessage processViewEvent(WxCpXmlMessage msg) {

        System.out.println("processViewEvent:\t" + msg.toString());
        return null;
    }

    /**
     * 流程模板发送工作完成
     *
     * @param msg 味精
     * @return {@link WxCpXmlMessage}
     */
    default WxCpXmlMessage processTemplateSendJobFinish(WxCpXmlMessage msg) {
        System.out.println("processTemplateSendJobFinish:\t" + msg.toString());
        return null;
    }


    /**
     * 自定义菜单事件
     *
     * @param msg
     * @return
     */
    default WxCpXmlMessage processClickEvent(WxCpXmlMessage msg) {
        System.out.println("processClickEvent:\t" + msg.toString());

        return null;
    }


    /**
     * 上报地理位置事件
     *
     * @param msg
     * @return
     */
    default WxCpXmlMessage processLocationEvent(WxCpXmlMessage msg) {
        System.out.println("processLocationEvent:\t" + msg.toString());
        return null;
    }


    /**
     * 扫描带参数二维码事件
     *
     * @param msg
     * @return
     */
    default WxCpXmlMessage processScanEvent(WxCpXmlMessage msg) {
        System.out.println("processScanEvent:\t" + msg.toString());
        return null;
    }


    /**
     * 取消关注事件
     *
     * @param msg
     * @return
     */
    default WxCpXmlMessage processUnsubscribeEvent(WxCpXmlMessage msg) {
        System.out.println("processUnsubscribeEvent:\t" + msg.toString());

        return null;
    }


    /**
     * 关注事件或者扫码关注事件，具体判断是否包含 eventKey
     *
     * @param msg
     * @return
     */
    default WxCpXmlMessage processSubscribeEvent(WxCpXmlMessage msg) {
        System.out.println("processSubscribeEvent:\t" + msg.toString());

        return null;
    }


    /**
     * 接收普通消息
     *
     * @param msg 微信推送过来的消息
     * @return 返回给微信的消息
     */
    default WxCpXmlMessage processTextMsg(WxCpXmlMessage msg) {

        System.out.println("processTextMsg:\t" + msg.toString());
        return null;
    }

    /**
     * 接收图片消息
     *
     * @param mp 微信推送过来的消息
     * @return 返回给微信的消息
     */
    default WxCpXmlMessage processImageMsg(WxCpXmlMessage mp) {

        System.out.println("processImageMsg:\t" + mp.toString());
        return null;
    }

    /**
     * 接收语音消息
     *
     * @param mp 微信推送过来的消息
     * @return 返回给微信的消息
     */
    default WxCpXmlMessage processVoiceMsg(WxCpXmlMessage mp) {

        System.out.println("processVoiceMsg:\t" + mp.toString());
        return null;
    }

    /**
     * 接收视频消息
     *
     * @param mp 微信推送过来的消息
     * @return 返回给微信的消息
     */
    default WxCpXmlMessage processVideoMsg(WxCpXmlMessage mp) {

        System.out.println("processVideoMsg:\t" + mp.toString());
        return null;
    }

    /**
     * 接收地理位置消息
     *
     * @param mp 微信推送过来的消息
     * @return 返回给微信的消息
     */
    default WxCpXmlMessage processLocationMsg(WxCpXmlMessage mp) {

        System.out.println("processLocationMsg:\t" + mp.toString());
        return null;
    }

    /**
     * 接收链接消息
     *
     * @param mp 微信推送过来的消息
     * @return 返回给微信的消息
     */
    default WxCpXmlMessage processLinkMsg(WxCpXmlMessage mp) {
        System.out.println("processLinkMsg:\t" + mp.toString());


        return null;
    }

    /**
     * 接收小视频消息
     *
     * @param mp 微信推送过来的消息
     * @return 返回给微信的消息
     */
    default WxCpXmlMessage processShortVideoMsg(WxCpXmlMessage mp) {

        System.out.println("processShortVideoMsg:\t" + mp.toString());
        return null;
    }
}
