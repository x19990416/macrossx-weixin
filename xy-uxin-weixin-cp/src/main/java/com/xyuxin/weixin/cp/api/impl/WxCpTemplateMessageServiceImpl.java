package com.xyuxin.weixin.cp.api.impl;

import com.google.gson.Gson;
import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.cp.WxCpConfig;
import com.xyuxin.weixin.cp.api.WxCpTemplateMessageService;
import com.xyuxin.weixin.cp.bean.message.WxCpMessage;
import com.xyuxin.weixin.cp.bean.message.WxCpMessageSendResult;

import java.io.IOException;

public class WxCpTemplateMessageServiceImpl extends BaseWxCpServiceImpl implements WxCpTemplateMessageService {
    public WxCpTemplateMessageServiceImpl(WxCpConfig config, HttpClientExecutor executorFactory) {
        super(config, executorFactory);
    }

    @Override
    public WxCpMessageSendResult send(WxCpMessage message) throws IOException {
        String url = String.format(WxCpTemplateMessageService.POST_MESSAGE_SEND, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        return new Gson().fromJson(executor.execute(url, message.toJson()), WxCpMessageSendResult.class);

    }
}
