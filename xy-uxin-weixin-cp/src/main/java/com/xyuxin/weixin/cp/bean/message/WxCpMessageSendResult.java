package com.xyuxin.weixin.cp.bean.message;

import com.google.gson.annotations.SerializedName;
import com.xyuxin.weixin.cp.bean.user.WxCpUserInvalidInvite;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WxCpMessageSendResult extends WxCpUserInvalidInvite {

    @SerializedName("msgid")
    private String msgId;

    @SerializedName("response_code")
    private String responseCode;

}
