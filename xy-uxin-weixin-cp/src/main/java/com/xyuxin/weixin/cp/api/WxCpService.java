package com.xyuxin.weixin.cp.api;

import java.io.IOException;

public interface WxCpService {

    String GET_ACCESS_TOKEN_URL = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=%s&corpsecret=%s";

    /**
     * 获取企业的access_token
     *
     * @return access_token
     * @see <a href="https://developer.work.weixin.qq.com/document/path/91039"></a>
     */
    String getAccessToken() throws IOException;


}
