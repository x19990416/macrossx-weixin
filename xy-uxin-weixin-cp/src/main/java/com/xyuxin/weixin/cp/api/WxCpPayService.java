package com.xyuxin.weixin.cp.api;

import com.xyuxin.weixin.common.service.WxService;
import com.xyuxin.weixin.cp.bean.pay.WxCpBillList;

import java.io.IOException;

/**
 * @see <a href="https://developer.work.weixin.qq.com/document/path/90273">企业支付</a>
 */
public interface WxCpPayService extends WxService {
    String POST_BILL_LIST = "https://qyapi.weixin.qq.com/cgi-bin/externalpay/get_bill_list?access_token=%s";

    /**
     * 获取对外收款记录
     *
     * @see <a href="https://developer.work.weixin.qq.com/document/path/93667"> 获取对外收款记录 </>
     * begin_time	是	收款记录开始时间戳，单位为秒
     * end_time	是	收款记录结束时间戳，单位为秒
     * payee_userid	否	企业收款成员userid，不填则为全部成员
     * cursor	否	用于分页查询的游标，字符串类型，由上一次调用返回，首次调用可不填
     * limit	否	返回的最大记录数，整型，最大值1000
     */
    WxCpBillList getBillList(long beginTimeSec, long endTimeSec, String payeeUserId, String cursor, Integer limit) throws IOException;


}
