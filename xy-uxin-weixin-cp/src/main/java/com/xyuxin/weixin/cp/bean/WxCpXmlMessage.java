package com.xyuxin.weixin.cp.bean;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.converters.enums.EnumSingleValueConverter;
import com.xyuxin.weixin.common.utils.xml.XStreamCDataConverter;
import com.xyuxin.weixin.cp.WxCpConstants;
import com.xyuxin.weixin.cp.util.XStreamTransformer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@XStreamAlias("xml")
public class WxCpXmlMessage {
    /**
     * 企业微信CorpID
     */
    @XStreamAlias("ToUserName")
    @XStreamConverter(XStreamCDataConverter.class)
    private String toUserName;

    /**
     * 接收的应用id，可在应用的设置页面获取
     */
    @XStreamAlias("AgentID")
    @XStreamConverter(XStreamCDataConverter.class)
    private String agentId;

    @XStreamAlias("Encrypt")
    @XStreamConverter(XStreamCDataConverter.class)
    private String encrypt;

    /**
     * 成员UserID
     */
    @XStreamAlias("FromUserName")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String fromUserName;
    /**
     * 消息创建时间（整型）
     */
    @XStreamAlias("CreateTime")
    private Long createTime;
    /**
     * 消息类型
     */
    @XStreamAlias("MsgType")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String msgType;

    /**
     * 文本消息内容
     */
    @XStreamAlias("Content")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String content;
    /**
     * 消息id，64位整型
     */
    @XStreamAlias("MsgId")
    private String msgId;


    /**
     * 图片链接 or 封面缩略图的url
     */
    @XStreamAlias("PicUrl")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String picUrl;

    /**
     * 图片媒体文件id，可以调用获取媒体文件接口拉取，仅三天内有效
     */
    @XStreamAlias("MediaId")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String mediaId;

    /**
     * 语音格式，如amr，speex等
     */
    @XStreamAlias("Format")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String format;

    /**
     * 视频消息缩略图的媒体id，可以调用获取媒体文件接口拉取数据，仅三天内有效
     */
    @XStreamAlias("ThumbMediaId")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String thumbMediaId;

    /**
     * 地理位置纬度
     */
    @XStreamAlias("Location_X")
    private Double locationX;
    /**
     * 地理位置经度
     */
    @XStreamAlias("Location_Y")
    private Double locationY;
    /**
     * 地图缩放大小
     */
    @XStreamAlias("Scale")
    private Double scale;

    /**
     * 地理位置信息
     */
    @XStreamAlias("Label")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String label;

    /**
     * app类型，在企业微信固定返回wxwork，在微信不返回该字段
     */
    @XStreamAlias("AppType")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String appType;
    /**
     * 标题
     */
    @XStreamAlias("Title")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String title;
    /**
     * 描述
     */
    @XStreamAlias("Description")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String description;

    /**
     * 链接跳转的url
     */
    @XStreamAlias("Url")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String url;
    @XStreamAlias("Event")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String event;
    @XStreamAlias("EventKey")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String eventKey;
    @XStreamAlias("Ticket")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String ticket;
    @XStreamAlias("Latitude")
    private Double latitude;
    @XStreamAlias("Longitude")
    private Double longitude;
    @XStreamAlias("Precision")
    private Double precision;
    @XStreamAlias("Recognition")
    @XStreamConverter(value = XStreamCDataConverter.class)
    private String recognition;
    @XStreamAlias("MsgSignature")
    @XStreamConverter(XStreamCDataConverter.class)
    private String msgSignature;
    @XStreamAlias("TimeStamp")
    private Long timestamp;
    @XStreamAlias("Nonce")
    @XStreamConverter(XStreamCDataConverter.class)
    private String nonce;
    @XStreamAlias("TemplateCard")
    private TemplateCard templateCard;

    @Data
    @Accessors(chain = true)
    public static class TemplateCard {
        @XStreamAlias("CardType")
        @XStreamConverter(XStreamCDataConverter.class)
        private String cardType;
        @XStreamAlias("CardType")
        private Source source;
        @XStreamAlias("MainTitle")
        private MainTitle mainTitle;
        @XStreamAlias("SubTitleText")
        @XStreamConverter(XStreamCDataConverter.class)
        private String subTitleText;
        @XStreamImplicit(itemFieldName = "HorizontalContentList")
        private List<HorizontalContent> horizontalContentList;
        @XStreamImplicit(itemFieldName = "JumpList")
        private List<JumpAction> jumpList;
        @XStreamAlias("CardAction")
        private Action cardAction;
        @XStreamAlias("EmphasisContent")
        private EmphasisContent emphasisContent;
        @XStreamAlias("ActionMenu")
        private ActionMenu actionMenu;
        @XStreamAlias("QuoteArea")
        private QuoteArea quoteArea;
        @XStreamImplicit(itemFieldName = "ButtonList")
        private List<Button> buttonList;
        @XStreamAlias("ReplaceText")
        private String replaceText;
        @XStreamAlias("ButtonSelection")
        private Selection buttonSelection;
        @XStreamAlias("CheckBox")
        private CheckBox checkBox;
        @XStreamAlias("SelectList")
        private Selection selectList;
        @XStreamAlias("SubmitButton")
        private Button submitButton;


        @Data
        @Accessors(chain = true)
        @EqualsAndHashCode(callSuper = true)
        public static class CheckBox extends Selection{
            /**
             * 选择题模式，单选：0，多选：1，不填或错填默认0
             */
            @XStreamAlias("Mode")
            private Integer mode;
        }

        @Data
        @Accessors(chain = true)
        public static class Selection {
            @XStreamAlias("QuestionKey")
            @XStreamConverter(XStreamCDataConverter.class)
            private String questionKey;
            @XStreamAlias("Title")
            @XStreamConverter(XStreamCDataConverter.class)
            private String title;
            @XStreamAlias("SelectedId")
            @XStreamConverter(XStreamCDataConverter.class)
            private String selectedId;
            @XStreamAlias("Disable")
            private Boolean disable;
            @XStreamImplicit(itemFieldName = "OptionList")
            private List<Option> optionList;

            @Data
            @Accessors(chain = true)
            public static class Option {
                @XStreamAlias("Id")
                @XStreamConverter(XStreamCDataConverter.class)
                private String id;
                @XStreamAlias("Text")
                @XStreamConverter(XStreamCDataConverter.class)
                private String text;
            }

        }

        @Data
        @Accessors(chain = true)
        public static class Button {
            @XStreamAlias("Text")
            @XStreamConverter(XStreamCDataConverter.class)
            private String text;
            @XStreamAlias("Style")
            private Integer style;
            @XStreamConverter(XStreamCDataConverter.class)
            @XStreamAlias("Key")
            private String key;
        }

        @Data
        @Accessors(chain = true)
        @EqualsAndHashCode(callSuper = true)
        public static class QuoteArea extends JumpAction {
            @XStreamAlias("QuoteText")
            private String quoteText;
        }

        @Data
        @Accessors(chain = true)
        public static class ActionMenu {
            @XStreamAlias("Desc")
            @XStreamConverter(XStreamCDataConverter.class)
            private String desc;
            @XStreamImplicit(itemFieldName = "ActionList")
            private List<Action> actionList;

            @Data
            @Accessors(chain = true)
            public static class Action {
                @XStreamConverter(XStreamCDataConverter.class)
                @XStreamAlias("Text")
                private String text;
                @XStreamAlias("Key")
                @XStreamConverter(XStreamCDataConverter.class)
                private String key;
            }
        }


        @Data
        @Accessors(chain = true)
        public static class EmphasisContent {
            @XStreamAlias("Title")
            @XStreamConverter(XStreamCDataConverter.class)
            private String title;
            @XStreamAlias("Desc")
            @XStreamConverter(XStreamCDataConverter.class)
            private String desc;
        }

        @Data
        @Accessors(chain = true)
        @EqualsAndHashCode(callSuper = true)
        public static class JumpAction extends Action {
            @XStreamAlias("Title")
            private String title;
        }

        @Data
        @Accessors(chain = true)
        public static class Action {
            @XStreamAlias("Type")
            private WxCpConstants.ActionType type;
            /**
             * 跳转链接的url，JumpList.Type是1时必填
             */
            @XStreamAlias("Url")
            private String url;
            /**
             * 跳转链接的小程序的appid，JumpList.Type是2时必填
             */
            @XStreamAlias("AppId")
            private String appId;
            /**
             * 跳转链接的小程序的pagepath，JumpList.Type是2时选填
             */
            @XStreamAlias("PagePath")
            private String pagePath;
        }

        @Data
        @Accessors(chain = true)
        public static class HorizontalContent {
            @XStreamAlias("KeyName")
            @XStreamConverter(XStreamCDataConverter.class)
            private String keyName;
            @XStreamAlias("Value")
            @XStreamConverter(XStreamCDataConverter.class)
            private String value;
            /**
             * 1 代表跳转url，2 代表下载附件，3 代表点击跳转成员详情
             */
            @XStreamAlias("Type")
            private WxCpConstants.HorizontalContentType type;
            /**
             * 链接跳转的url，HorizontalContentList.Type是1时必填
             */
            @XStreamAlias("Url")
            @XStreamConverter(XStreamCDataConverter.class)
            private String url;
            /**
             * 附件的media_id，HorizontalContentList.Type是2时必填
             */
            @XStreamAlias("MediaId")
            @XStreamConverter(XStreamCDataConverter.class)
            private String mediaId;
            /**
             * 成员详情的userid，HorizontalContentList.Type是3时必填
             */
            @XStreamAlias("UserId")
            @XStreamConverter(XStreamCDataConverter.class)
            private String userId;

        }

        @Data
        @Accessors(chain = true)
        public static class MainTitle {
            @XStreamAlias("Title")
            private String title;
            @XStreamAlias("Desc")
            private String desc;
        }


        @Data
        @Accessors(chain = true)
        public static class Source {
            @XStreamAlias("IconUrl")
            @XStreamConverter(XStreamCDataConverter.class)
            private String iconUrl;
            @XStreamAlias("Desc")
            @XStreamConverter(XStreamCDataConverter.class)
            private String desc;
            @XStreamAlias("DescColor")
            private Integer descColor;
        }

    }

    public static void main(String... s) {
        WxCpXmlMessage message = new WxCpXmlMessage();
        TemplateCard card = new TemplateCard();
        card.setHorizontalContentList(List.of(new TemplateCard.HorizontalContent().setMediaId("aaa").setType(WxCpConstants.HorizontalContentType.Media), new TemplateCard.HorizontalContent()));
        card.setSource(new TemplateCard.Source().setDesc("a").setDescColor(1).setIconUrl("b"));
        card.setQuoteArea(new TemplateCard.QuoteArea().setQuoteText("text"));
        message.setTemplateCard(card);


        System.out.println(XStreamTransformer.toXml(WxCpXmlMessage.class, message));
    }
}
