package com.xyuxin.weixin.cp.bean.user;

import com.google.gson.annotations.SerializedName;
import com.xyuxin.weixin.common.bean.WxErr;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 企业成员的userid与对应的部门ID列表
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WxCpDeptUserResult extends WxErr {
    /**
     * 分页游标，下次请求时填写以获取之后分页的记录。如果该字段返回空则表示已没有更多数据
     */
    @SerializedName("next_cursor")
    private String nextCursor;
    @SerializedName("dept_user")
    private List<DeptUser> deptUser;

    @Data
    public static class DeptUser {
        /**
         * 用户userid，当用户在多个部门下时会有多条记录
         */
        @SerializedName("userid")
        private String userId;
        /**
         * 用户所属部门
         */
        @SerializedName("department")
        private Long department;
    }

}
