package com.xyuxin.weixin.cp.bean.miniapppay;

import com.google.gson.annotations.SerializedName;
import com.xyuxin.weixin.common.bean.WxErr;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class BillDownloadInfo extends WxErr {
    @SerializedName("download_url")
    private String downloadUrl;
    @SerializedName("hash_type")
    private String hashType;
    @SerializedName("hash_value")
    private String hashValue;
    @SerializedName("auth")
    private String auth;
}
