package com.xyuxin.weixin.cp.bean.pay;

/**
 * 联系人信息(contact)（第三方不可获取）:
 */
public class WxCpContact {
    private String name;
    private String phone;
    private String address;
}
