package com.xyuxin.weixin.cp.bean.templatecard;

import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActionMenuItem  {
    /**
     * 操作的描述文案
     */
    private String text;

    /**
     * 按钮key值，用户点击后，会产生回调事件将本参数作为EventKey返回，回调事件会带上该key值，最长支持1024字节，不可重复
     */
    private String key;

    /**
     * To json json object.
     *
     * @return the json object
     */
    public JsonObject toJson() {
        JsonObject btnObject = new JsonObject();
        btnObject.addProperty("text", this.getText());
        btnObject.addProperty("key", this.getKey());
        return btnObject;
    }

}
