package com.xyuxin.weixin.cp.bean.media;

import com.google.gson.annotations.SerializedName;
import com.xyuxin.weixin.common.bean.WxErr;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 上传临时素材结果
 * @author guolimin
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WxMediaUploadResult extends WxErr {
    /**
     * 媒体文件类型，分别有图片（image）、语音（voice）、视频（video），普通文件(file)
     */
    @SerializedName("type")

    private String type;
    /**
     * 媒体文件上传后获取的唯一标识，3天内有效
     */
    @SerializedName("media_id")

    private String mediaId;
    /**
     * 媒体文件上传时间戳
     */
    @SerializedName("create_at")

    private String createdAt;
}
