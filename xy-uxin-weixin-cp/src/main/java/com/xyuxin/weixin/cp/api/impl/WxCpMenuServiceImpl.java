package com.xyuxin.weixin.cp.api.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.xyuxin.weixin.common.bean.WxErr;
import com.xyuxin.weixin.common.utils.http.BaseHttpGetExecutor;
import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.cp.WxCpConfig;
import com.xyuxin.weixin.cp.api.WxCpMediaService;
import com.xyuxin.weixin.cp.api.WxCpMenuService;
import com.xyuxin.weixin.cp.bean.menu.WxCpMenuInfo;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
@Slf4j
public class WxCpMenuServiceImpl extends BaseWxCpServiceImpl implements WxCpMenuService {
    public WxCpMenuServiceImpl(WxCpConfig config, HttpClientExecutor executorFactory) {
        super(config, executorFactory);
    }

    @Override
    public boolean create(WxCpMenuInfo menuInfo) throws IOException {
        String url = String.format(WxCpMenuService.POST_MENU_CREATE, super.getAccessToken(),super.getConfig().getAgientId());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        String content = executor.execute(url,new Gson().toJson(menuInfo));
        WxErr err =new Gson().fromJson(content, WxErr.class);
        if(err.getErrCode() == 0){
            return true;
        }else{
            log.error("create menu error for [{}]",err);
            return false;
        }
    }

    @Override
    public WxCpMenuInfo get() throws IOException {
        String url = String.format(WxCpMenuService.GET_MENU_GET, super.getAccessToken(),super.getConfig().getAgientId());
        BaseHttpGetExecutor<String> executor = super.getExecutorFactory().httpGetStringExecutor();
        String content = executor.execute(url,null);
        return new Gson().fromJson(content, WxCpMenuInfo.class);
    }

    @Override
    public boolean delete() throws IOException {
        String url = String.format(WxCpMenuService.GET_MENU_DELETE, super.getAccessToken(),super.getConfig().getAgientId());
        BaseHttpGetExecutor<String> executor = super.getExecutorFactory().httpGetStringExecutor();
        String content = executor.execute(url,null);
        WxErr err =new Gson().fromJson(content, WxErr.class);
        if(err.getErrCode() == 0){
            return true;
        }else{
            log.error("delete menu error for [{}]",err);
            return false;
        }
    }
}
