package com.xyuxin.weixin.cp.bean;

import lombok.Getter;

public enum GroupChatStatusFilter {
    ALL("所有列表(即不过滤)", 0),
    /**
     * 男
     */
    PENDING_INHERITANCE("离职待继承", 1),
    /**
     * 女
     */
    INHERITING("离职继承中", 2),
    INHERITANCE_COMPLETED("离职继承完成", 3);
    @Getter
    private final String text;
    @Getter
    private final Integer code;

    GroupChatStatusFilter(String text, Integer code) {
        this.text = text;
        this.code = code;
    }

}
