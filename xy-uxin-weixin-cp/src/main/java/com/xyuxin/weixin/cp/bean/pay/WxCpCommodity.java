package com.xyuxin.weixin.cp.bean.pay;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 商品信息详情
 */
@Data
public class WxCpCommodity {
    private String description;
    private BigDecimal amount;
}
