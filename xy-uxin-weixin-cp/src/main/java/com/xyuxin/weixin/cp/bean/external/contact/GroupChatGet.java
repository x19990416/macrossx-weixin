package com.xyuxin.weixin.cp.bean.external.contact;

import com.google.gson.annotations.SerializedName;
import com.xyuxin.weixin.common.bean.WxErr;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class GroupChatGet extends WxErr {
    @SerializedName("group_chat")
    private GroupChat groutChat;

    @Data
    public static class GroupChat {
        @SerializedName("chat_id")
        private String chatId;
        private String name;
        private String owner;
        @SerializedName("create_time")
        private Long createTime;
        private String notice;
        @SerializedName("member_list")
        private List<GroupChatMember> memberList;
        @SerializedName("admin_list")
        private List<GroupChatMember> adminList;
        @SerializedName("member_version")
        private String memberVersion;
    }

    @Data
    public static class GroupChatMember {
        @SerializedName("userid")
        private String userId;
        private Integer type;
        @SerializedName("join_time")
        private Long joinTime;
        private Integer joinScene;
        private GroupChatMemberInvitor invitor;
        @SerializedName("group_nickname")
        private String groupNickname;
        private String name;
    }

    @Data
    public static class GroupChatMemberInvitor {
        @SerializedName("userid")
        private String userId;
    }
}
