package com.xyuxin.weixin.cp.bean.user;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 企业可通过接口批量邀请成员使用企业微信，邀请后将通过短信或邮件下发通知。
 * @author guolimin
 * @see <a href="https://developer.work.weixin.qq.com/document/path/90975">邀请成员</a>
 */
@Data
@Accessors(chain = true)
public class WxCpUserInvite {
    /**
     * 成员ID列表, 最多支持1000个。
     */
    private List<String> user;
    /**
     * 部门ID列表，最多支持100个。
     */
    private List<String> party;
    /**
     * 标签ID列表，最多支持100个。
     */
    private List<String> tag;
}
