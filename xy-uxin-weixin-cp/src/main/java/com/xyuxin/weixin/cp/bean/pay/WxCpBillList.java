package com.xyuxin.weixin.cp.bean.pay;

import com.xyuxin.weixin.common.bean.WxErr;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
public class WxCpBillList extends WxErr {
    private String nextCursor;
    private List<WxCpBill> billList;
}
