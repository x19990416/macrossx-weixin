package com.xyuxin.weixin.cp.bean.templatecard;

import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HorizontalContent {

    /**
     * 链接类型，0或不填代表不是链接，1 代表跳转url，2 代表下载附件
     */
    private Integer type;
    /**
     * 二级标题，建议不超过5个字
     */
    private String keyname;
    /**
     * 二级文本，如果horizontal_content_list.type是2，该字段代表文件名称（要包含文件类型），建议不超过30个字
     */
    private String value;
    /**
     * 链接跳转的url，horizontal_content_list.type是1时必填
     */
    private String url;
    /**
     * 附件的media_id，horizontal_content_list.type是2时必填
     */
    private String media_id;

    /**
     * 成员详情的userid，horizontal_content_list.type是3时必填
     */
    private String userid;

    /**
     * To json json object.
     *
     * @return the json object
     */
    public JsonObject toJson() {
        JsonObject hContentJson = new JsonObject();

        if (null != this.getType()) {
            hContentJson.addProperty("type", this.getType());
        }
        hContentJson.addProperty("keyname", this.getKeyname());

        if (StringUtils.isNotBlank(this.getValue())) {
            hContentJson.addProperty("value", this.getValue());
        }
        if (StringUtils.isNotBlank(this.getUrl())) {
            hContentJson.addProperty("url", this.getUrl());
        }
        if (StringUtils.isNotBlank(this.getMedia_id())) {
            hContentJson.addProperty("media_id", this.getMedia_id());
        }
        if (StringUtils.isNotBlank(this.getUserid())) {
            hContentJson.addProperty("userid", this.getUserid());
        }
        return hContentJson;
    }

}
