package com.xyuxin.weixin.cp.util;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.xyuxin.weixin.common.utils.SignUtil;
import com.xyuxin.weixin.common.utils.xml.XStreamInitializer;
import com.xyuxin.weixin.cp.WxCpConstants;
import com.xyuxin.weixin.cp.bean.WxCpXmlMessage;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class XStreamTransformer {

    /**
     * The constant CLASS_2_XSTREAM_INSTANCE.
     */
    protected static final Map<Class, XStream> CLASS_2_XSTREAM_INSTANCE = configXStreamInstance();

    /**
     * xml -> pojo
     *
     * @param <T>   the type parameter
     * @param clazz the clazz
     * @param xml   the xml
     * @return the t
     */
    @SuppressWarnings("unchecked")
    public static <T> T fromXml(Class<T> clazz, String xml) {
        T object = (T) CLASS_2_XSTREAM_INSTANCE.get(clazz).fromXML(xml);
        return object;
    }

    /**
     * From xml t.
     *
     * @param <T>   the type parameter
     * @param clazz the clazz
     * @param is    the is
     * @return the t
     */
    @SuppressWarnings("unchecked")
    public static <T> T fromXml(Class<T> clazz, InputStream is) {
        T object = (T) CLASS_2_XSTREAM_INSTANCE.get(clazz).fromXML(is);
        return object;
    }

    /**
     * 注册扩展消息的解析器.
     *
     * @param clz     类型
     * @param xStream xml解析器
     */
    public static void register(Class clz, XStream xStream) {
        CLASS_2_XSTREAM_INSTANCE.put(clz, xStream);
    }

    /**
     * pojo -> xml.
     *
     * @param <T>    the type parameter
     * @param clazz  the clazz
     * @param object the object
     * @return the string
     */
    public static <T> String toXml(Class<T> clazz, T object) {
        return CLASS_2_XSTREAM_INSTANCE.get(clazz).toXML(object);
    }




        private static Map<Class, XStream> configXStreamInstance() {
            Map<Class, XStream> map = new HashMap<>();
            map.put(WxCpXmlMessage.class, configWxCpXmlMessage());
//        map.put(WxCpXmlOutNewsMessage.class, configWxCpXmlOutNewsMessage());
//        map.put(WxCpXmlOutTextMessage.class, configWxCpXmlOutTextMessage());
//        map.put(WxCpXmlOutImageMessage.class, configWxCpXmlOutImageMessage());
//        map.put(WxCpXmlOutVideoMessage.class, configWxCpXmlOutVideoMessage());
//        map.put(WxCpXmlOutVoiceMessage.class, configWxCpXmlOutVoiceMessage());
//        map.put(WxCpXmlOutTaskCardMessage.class, configWxCpXmlOutTaskCardMessage());
//        map.put(WxCpXmlOutUpdateBtnMessage.class, configWxCpXmlOutUpdateBtnMessage());
//        map.put(WxCpTpXmlPackage.class, configWxCpTpXmlPackage());
//        map.put(WxCpTpXmlMessage.class, configWxCpTpXmlMessage());
//        map.put(WxCpXmlOutEventMessage.class, configWxCpXmlOutEventMessage());
            return map;
        }


        private static XStream configWxCpXmlMessage() {
            XStream xstream = XStreamInitializer.getInstance();
            xstream.processAnnotations(WxCpXmlMessage.class);
            xstream.registerConverter(new HorizontalContentTypeConverter());
//        xstream.processAnnotations(WxCpXmlMessage.ScanCodeInfo.class);
//        xstream.processAnnotations(WxCpXmlMessage.SendPicsInfo.class);
//        xstream.processAnnotations(WxCpXmlMessage.SendPicsInfo.Item.class);
//        xstream.processAnnotations(WxCpXmlMessage.SendLocationInfo.class);
            return xstream;
        }

        private static XStream configWxCpXmlOutImageMessage() {
            XStream xstream = XStreamInitializer.getInstance();

//        xstream.processAnnotations(WxCpXmlOutMessage.class);
//        xstream.processAnnotations(WxCpXmlOutImageMessage.class);
            return xstream;
        }

        private static XStream configWxCpXmlOutNewsMessage() {
            XStream xstream = XStreamInitializer.getInstance();

//        xstream.processAnnotations(WxCpXmlOutMessage.class);
//        xstream.processAnnotations(WxCpXmlOutNewsMessage.class);
//        xstream.processAnnotations(WxCpXmlOutNewsMessage.Item.class);
            return xstream;
        }


        public static class HorizontalContentTypeConverter implements Converter {

            @Override
            public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
                WxCpConstants.HorizontalContentType horizontalContentType = (WxCpConstants.HorizontalContentType) source;
                writer.setValue(String.valueOf(horizontalContentType.getType()));
            }

            @Override
            public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
                int type = Integer.parseInt(reader.getValue());
                return switch (type) {
                    case 0 -> WxCpConstants.HorizontalContentType.None;
                    case 1 -> WxCpConstants.HorizontalContentType.Url;
                    case 2 -> WxCpConstants.HorizontalContentType.Media;
                    case 3 -> WxCpConstants.HorizontalContentType.UserId;
                    default -> throw new IllegalArgumentException("Invalid HorizontalContentType type: " + type);
                };
            }

            @Override
            public boolean canConvert(Class type) {
                return type == WxCpConstants.HorizontalContentType.class;
            }
        }
        private static XStream configWxCpXmlOutTextMessage() {
            XStream xstream = XStreamInitializer.getInstance();

//        xstream.processAnnotations(WxCpXmlOutMessage.class);
//        xstream.processAnnotations(WxCpXmlOutTextMessage.class);
            return xstream;
        }

        private static XStream configWxCpXmlOutVideoMessage() {
            XStream xstream = XStreamInitializer.getInstance();

//        xstream.processAnnotations(WxCpXmlOutMessage.class);
//        xstream.processAnnotations(WxCpXmlOutVideoMessage.class);
//        xstream.processAnnotations(WxCpXmlOutVideoMessage.Video.class);
            return xstream;
        }

        private static XStream configWxCpXmlOutVoiceMessage() {
            XStream xstream = XStreamInitializer.getInstance();

//        xstream.processAnnotations(WxCpXmlOutMessage.class);
//        xstream.processAnnotations(WxCpXmlOutVoiceMessage.class);
            return xstream;
        }

        private static XStream configWxCpXmlOutTaskCardMessage() {
            XStream xstream = XStreamInitializer.getInstance();

//        xstream.processAnnotations(WxCpXmlOutMessage.class);
//        xstream.processAnnotations(WxCpXmlOutTaskCardMessage.class);
            return xstream;
        }

        private static XStream configWxCpXmlOutUpdateBtnMessage() {
            XStream xstream = XStreamInitializer.getInstance();
//        xstream.processAnnotations(WxCpXmlOutMessage.class);
//        xstream.processAnnotations(WxCpXmlOutUpdateBtnMessage.class);
            return xstream;
        }

        private static XStream configWxCpTpXmlPackage() {
            XStream xstream = XStreamInitializer.getInstance();
            //xstream.processAnnotations(WxCpTpXmlPackage.class);

            return xstream;
        }

        private static XStream configWxCpTpXmlMessage() {
            XStream xstream = XStreamInitializer.getInstance();
            //xstream.processAnnotations(WxCpTpXmlMessage.class);

            return xstream;
        }

        private static XStream configWxCpXmlOutEventMessage() {
            XStream xstream = XStreamInitializer.getInstance();
            // xstream.processAnnotations(WxCpXmlOutMessage.class);
            // xstream.processAnnotations(WxCpXmlOutEventMessage.class);
            return xstream;
        }

        public static void main(String... s) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
            String code = "dQDhzb6KZ1nsaG9xmjtuHmvhu6BfzlKRi4q6GOpAJT2JMwszYm/tVKrovgHXYU861IcGVao/7RaGARLy/cGGIw==";
            String key = "x759mDspbsDbh6UZooKR2Iw6ra0dzKRq0dDozDsgS5z=";
            String token = "fcLqKAWZke3LcLxQ";
            String baseStr = "<xml>\n" + "  <Encrypt><![CDATA[iCHidadPFWhwtUk0CkstYNOCJGgDYfbrULI06Tz/VJkIH+O7leGYDpFwmo5ZAAZMHZl3fIIUWRgJHg9eq7fenPKXY9WXVwnXxnUpqmTDJZimputcKTSAHZWtXYM90WEv7MN9FtjT5KSqhGef32qG6tHIyMOBdVKXEJ+Ape3ihL6t5b7vbpgJ508VO+VF1wzsTuJiyAA6NEySvAST\n" + "7Z+OL5bCanK7zyw8PaQr2ZrKepySF9jy+qiK9G/K87OrsgUoB0J5Y7RXHjOPA12cac7gtGf2aEZUsFVZ4b47mNPQwPqK5WzuwmrzEcHrqILxCi7Fh/xTysDf6pAruSPeQ+NXJcxlDyAjhWKLqSXbYF+oi38=]]></Encrypt>\n" + "  <MsgSignature><![CDATA[a240b256a24c7287bd43598a09387280574b3fe3]]></MsgSignature>\n" + "  <TimeStamp>1699423369407</TimeStamp>\n" + "  <Nonce><![CDATA[UtYM3Zp1]]></Nonce>\n" + "</xml>\n";
            WxCpXmlMessage base = XStreamTransformer.fromXml(WxCpXmlMessage.class, baseStr);
            String sign = SignUtil.sign(base.getEncrypt(), base.getNonce(), token, String.valueOf(base.getTimestamp()));
            System.out.println(sign);
            System.out.println(base);
            //@NotEmpty String token, @NotEmpty String timestamp,@NotEmpty String nonce,@NotEmpty String encryptStr
            System.out.println(SignUtil.wxSignCheck(token, base.getTimestamp().toString(), base.getNonce(), base.getEncrypt(), base.getMsgSignature()));
        }

    }
