/*
 * Copyright (c) 2021, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.cp;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 微信企业号配置类
 *
 * @author Guo Limin
 * @date 2023/10/27
 */
@Data
@Accessors(chain = true)
public class WxCpConfig {
    /**
     * 企业ID
     */
    private String corpid;

    /**
     * 应用的凭证密钥
     */
    private String corpsecret;
    /**
     * 应用ID
     */
    private String agientId;
}
