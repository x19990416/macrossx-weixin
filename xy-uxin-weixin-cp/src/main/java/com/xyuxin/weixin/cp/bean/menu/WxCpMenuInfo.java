package com.xyuxin.weixin.cp.bean.menu;

import com.google.gson.annotations.SerializedName;
import com.xyuxin.weixin.common.bean.WxErr;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Collection;

@Data
@EqualsAndHashCode(callSuper = true)
public class WxCpMenuInfo extends WxErr {
    Collection<MenuButton> button;

    @Data
    public static class MenuButton {
        private String type;
        private String name;
        private String key;
        private String url;
        @SerializedName("sub_button")
        Collection<MenuButton> subButton;
        @SerializedName("pagepath")
        private String pagePath;
        @SerializedName("appid")
        private String appId;

    }
}
