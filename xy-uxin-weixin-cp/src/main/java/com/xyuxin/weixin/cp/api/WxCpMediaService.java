package com.xyuxin.weixin.cp.api;

import com.xyuxin.weixin.common.utils.http.bean.BaseFileEntity;
import com.xyuxin.weixin.cp.WxCpConstants;
import com.xyuxin.weixin.cp.bean.media.WxMediaUploadResult;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * 素材管理
 *
 * @author guolimin
 * @see <a href="https://developer.work.weixin.qq.com/document/path/91197">素材管理</a>
 */
public interface WxCpMediaService {
    String POST_MEDIA_UPLOAD = "https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s";

    /**
     * 上传临时素材
     * 素材上传得到media_id，该media_id仅三天内有效, media_id在同一企业内应用之间可以共享
     *
     * @param type WxCpConstants.MsgAuditMediaType
     * @see <a href="https://developer.work.weixin.qq.com/document/path/90389">上传临时素材</a>
     */
    WxMediaUploadResult upload(BaseFileEntity entity, String type) throws IOException;

    String POST_MEDIA_UPLOAD_IMG = "https://qyapi.weixin.qq.com/cgi-bin/media/uploadimg?access_token=%s";

    /**
     * 上传图片
     * 上传图片得到图片URL，该URL永久有效
     * 返回的图片URL，仅能用于图文消息正文中的图片展示，或者给客户发送欢迎语等；若用于非企业微信环境下的页面，图片将被屏蔽。
     * 每个企业每月最多可上传3000张图片，每天最多可上传1000张图片
     * @see <a href="https://developer.work.weixin.qq.com/document/path/90392">上传图片</a>
     */
    String uploadImg(File imgFile) throws IOException;

    String GET_MEDIA = "https://qyapi.weixin.qq.com/cgi-bin/media/get?access_token=%s&media_id=%s";

    /**
     * 获取临时素材的文件的url
     * media_id 媒体文件id，见上传临时素材，以及异步上传临时素材（超过20M需使用Range分块下载，且分块大小不超过20M，否则返回错误码830002）
     * @see <a href="https://developer.work.weixin.qq.com/document/path/90390">获取临时素材</a>
     */
    InputStream getStream(String mediaId) throws IOException;
    String getUrl(String mediaId) throws IOException;
    String GET_MEDIA_JSSDK = "https://qyapi.weixin.qq.com/cgi-bin/media/get/jssdk?access_token=%s&media_id=%s";

    /**
     * 获取高清语音素材 url
     * 可以使用本接口获取从JSSDK的uploadVoice接口上传的临时语音素材，格式为speex，16K采样率。该音频比上文的临时素材获取接口（格式为amr，8K采样率）更加清晰，适合用作语音识别等对音质要求较高的业务。
     * @see <a href="https://developer.work.weixin.qq.com/document/path/90391">获取高清语音素材</a>
     */
    String getJssdk(String mediaId) throws IOException;

}
