/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp;

/**
 * @author Guo Limin
 */
public class WxMpConstants {

    public static enum BasicMsgType {
        TEXT("text"), IMAGE("image"), VOICE("voice"), VIDEO("video"), SHORT_VIDEO("shortvideo"), LOCATION("location"), LINK("link"), EVENT("event");
        private final String type;

        BasicMsgType(String type) {
            this.type = type;
        }
    }

    public static enum BasicEventMsgType {
        SUBSCRIBE("subscribe"), UNSUBSCRIBE("unsubscribe"), LOCATION("LOCATION"), SCAN("SCAN"), CLICK("CLICK"), VIEW(
                "VIEW"),TEMPLATESENDJOBFINISH("TEMPLATESENDJOBFINISH");
        private final String type;

        private BasicEventMsgType(String type) {
            this.type = type;
        }
    }

    public static enum QrCodeActionName {
        /**
         * 为临时的整型参数值
         */
        QR_SCENE,
        /**
         * 为临时的字符串参数值
         */
        QR_STR_SCENE,
        /**
         * 为永久的整型参数值
         */
        QR_LIMIT_SCENE,
        /**
         * 为永久的字符串参数值
         */
        QR_LIMIT_STR_SCENE
    }


    /**
     * 返回用户关注的渠道来源
     */
    public static enum SubscribeSceneType {
        /**
         * 公众号搜索
         */
        ADD_SCENE_SEARCH("ADD_SCENE_SEARCH"),
        /**
         * 公众号迁移
         */
        ADD_SCENE_ACCOUNT_MIGRATION("ADD_SCENE_ACCOUNT_MIGRATION"),
        /**
         * 名片分享
         */
        ADD_SCENE_PROFILE_CARD("ADD_SCENE_PROFILE_CARD"),
        /**
         * 扫描二维码
         */
        ADD_SCENE_QR_CODE("ADD_SCENE_QR_CODE"),
        /**
         * 图文页内名称点
         */
        ADD_SCENE_PROFILE_LINK("ADD_SCENE_PROFILE_LINK"),
        /**
         * 图文页右上角菜单
         */
        ADD_SCENE_PROFILE_ITEM("ADD_SCENE_PROFILE_ITEM"),
        /**
         * 支付后关注
         */
        ADD_SCENE_PAID("ADD_SCENE_PAID"),
        /**
         * 微信广告
         */
        ADD_SCENE_WECHAT_ADVERTISEMENT("ADD_SCENE_WECHAT_ADVERTISEMENT"),
        /**
         * 他人转载
         */
        ADD_SCENE_REPRINT("ADD_SCENE_REPRINT"),
        /**
         * 视频号直播
         */
        ADD_SCENE_LIVESTREAM("ADD_SCENE_LIVESTREAM"),
        /**
         * 视频号
         */
        ADD_SCENE_CHANNELS("ADD_SCENE_CHANNELS"),
        /**
         * 其他
         */
        ADD_SCENE_OTHERS("ADD_SCENE_OTHERS");
        private final String type;

        private SubscribeSceneType(String type) {
            this.type = type;
        }
    }

}
