/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.api.impl;

import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.mp.WxMpConfig;
import com.xyuxin.weixin.mp.WxMpConstants;
import com.xyuxin.weixin.mp.api.WxMpAccountService;
import com.xyuxin.weixin.mp.bean.account.WxParametricQrCode;
import com.xyuxin.weixin.mp.bean.account.WxParametricQrCodeAction;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * @author Guo Limin
 */
@Slf4j
public class WxMpAccountServiceImpl extends BaseWxMpServiceImpl implements WxMpAccountService {
    public WxMpAccountServiceImpl(WxMpConfig config, HttpClientExecutor executorFactory) {
        super(config, executorFactory);
    }

    @Override
    public WxParametricQrCode generateParametricQrCode(WxMpConstants.QrCodeActionName actionName, Long expireSeconds, Long sceneId, String sceneStr) throws IOException {
        String url = String.format(WxMpAccountService.POST_USER_INFO, super.getAccessToken());
        WxParametricQrCodeAction action = new WxParametricQrCodeAction().setActionName(actionName.name()).setExpireSeconds(expireSeconds).setActionInfo(new WxParametricQrCodeAction.ActionInfo().setScene(new WxParametricQrCodeAction.Scene().setSceneId(sceneId).setSceneStr(sceneStr)));
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        WxParametricQrCode qrcode = new Gson().fromJson(executor.execute(url, new Gson().toJson(action)), WxParametricQrCode.class);
        return qrcode;
    }
}
