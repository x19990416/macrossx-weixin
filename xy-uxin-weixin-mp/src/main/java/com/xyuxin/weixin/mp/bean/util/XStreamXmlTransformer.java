/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.bean.util;


import com.xyuxin.weixin.common.utils.xml.XStreamInitializer;
import com.xyuxin.weixin.mp.bean.WxMpXmlMessage;
import com.thoughtworks.xstream.XStream;

import java.io.InputStream;
import java.util.*;

/**
 * @author Guo Limin
 */
public class XStreamXmlTransformer {
    private static final Map<Class<?>, XStream> _CALSS_2_XSTREAM_INSTANCE = new HashMap<>();

    static {
        registerClass(WxMpXmlMessage.class);
    }

    public static <T> T fromXml(Class<T> clazz, String xml) {
        return (T) _CALSS_2_XSTREAM_INSTANCE.get(clazz).fromXML(xml);
    }

    public static <T> T fromXml(Class<T> clazz, InputStream inputStream) {
        return (T) _CALSS_2_XSTREAM_INSTANCE.get(clazz).fromXML(inputStream);
    }

    public static <T> String toXml(T obj) {
        return _CALSS_2_XSTREAM_INSTANCE.get(obj.getClass()).toXML(obj);
    }

    public static <T> String toXml(Class<T> clazz, T object) {
        return _CALSS_2_XSTREAM_INSTANCE.get(clazz).toXML(object);
    }


    private static void registerClass(Class<?> clazz) {
        XStream xstream = XStreamInitializer.getInstance();
        xstream.processAnnotations(clazz);
        xstream.processAnnotations(getInnerClasses(clazz));
        register(clazz, xstream);

    }

    public static void register(Class<?> clz, XStream xStream) {
        _CALSS_2_XSTREAM_INSTANCE.put(clz, xStream);
    }


    private static Class<?>[] getInnerClasses(Class<?> clazz) {
        Class<?>[] innerClasses = clazz.getClasses();
        if (innerClasses == null) {
            return null;
        }

        List<Class<?>> result = new ArrayList<>();
        result.addAll(Arrays.asList(innerClasses));
        for (Class<?> inner : innerClasses) {
            Class<?>[] innerClz = getInnerClasses(inner);
            if (innerClz == null) {
                continue;
            }

            result.addAll(Arrays.asList(innerClz));
        }

        return result.toArray(new Class<?>[0]);
    }
}
