package com.xyuxin.weixin.mp.api;

import com.xyuxin.weixin.mp.bean.template.WxMpTemplateMessage;

import java.io.IOException;

/**
 * 微信消息模板服务
 *
 * @author 50014792@qq.com
 * @date 2023/06/27
 */
public interface WxMpTemplateMessageService {

    String POST_TEMPLATE_SEND = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=%s";

    /**
     * 发送模板信息
     *
     * @param templateMessage 模板信息
     * @return {@link String}
     * @throws IOException ioexception
     */
    String sendTemplateMessage(WxMpTemplateMessage templateMessage) throws IOException;
}
