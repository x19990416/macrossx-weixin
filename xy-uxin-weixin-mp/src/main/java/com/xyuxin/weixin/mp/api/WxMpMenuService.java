/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.api;

import com.xyuxin.weixin.common.bean.menu.WxMenu;
import com.xyuxin.weixin.mp.bean.menu.WxMpConditionalMenu;
import com.xyuxin.weixin.mp.bean.menu.WxMpSelfMenuInfoResult;

import java.io.IOException;

/**
 * 自定义菜单
 *
 * @author Guo Limin
 * @see <a href="https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Creating_Custom-Defined_Menu.html">自定义菜单</a>
 */
public interface WxMpMenuService {

    /**
     * 创建菜单
     *
     * @See <a href="https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Creating_Custom-Defined_Menu.html">创建菜单</a>
     */
    String POST_CREATE_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=%s";

    boolean createMenu(WxMenu menu) throws IOException;

    /**
     * 查询菜单
     *
     * @See <a href="https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Querying_Custom_Menus.html">查询菜单</a>
     */
    String GET_QUERY_MENU_URL = "https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=%s";

    WxMpSelfMenuInfoResult queryMenuInfo() throws IOException;

    /**
     * 删除菜单
     *
     * @See <a href="https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Deleting_Custom-Defined_Menu.html">删除菜单</a>
     */
    String GET_DEL_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=%s";

    boolean deleteMenu() throws IOException;

    /**
     * 创建个性化菜单
     *
     * @see <a href="https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Personalized_menu_interface.html">创建个性化菜单</a>
     */
    String POST_CREATE_CONDITIONAL_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=%s";

    public boolean createConditionalMenu(WxMenu menu) throws IOException;

    /**
     * 获取自定义菜单配置
     *
     * @see <a href="https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Getting_Custom_Menu_Configurations.html"/>获取个性化菜单</a>
     */
    String GET_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=%s";

    public WxMpConditionalMenu getMenu() throws IOException;
}
