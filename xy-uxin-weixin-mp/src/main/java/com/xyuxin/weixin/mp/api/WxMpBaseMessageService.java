/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.api;

import com.xyuxin.weixin.mp.WxMpConstants;
import com.xyuxin.weixin.mp.bean.WxMpXmlMessage;

import javax.validation.constraints.NotNull;

/**
 * 微信基础消息能力，根据具体业务实现各自的消息
 *
 * @author Guo Limin
 * @see <a href="https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Receiving_standard_messages.html">基础消息能力<a/>
 */
public interface WxMpBaseMessageService {
    /**
     * 处理微信消息，默认自动设置 toUserName 和 fromUserName
     *
     * @param msg 微信传过来的消息
     * @return 传回给微信的消息，null 表示消息类型不存在或不处理。
     */
    default WxMpXmlMessage process(@NotNull WxMpXmlMessage msg) {
        WxMpXmlMessage result = switch (WxMpConstants.BasicMsgType.valueOf(msg.getMsgType().toUpperCase())) {
            case TEXT -> processTextMsg(msg);
            case IMAGE -> processImageMsg(msg);
            case LINK -> processLinkMsg(msg);
            case LOCATION -> processLocationMsg(msg);
            case SHORT_VIDEO -> processShortVideoMsg(msg);
            case VIDEO -> processVideoMsg(msg);
            case VOICE -> processVoiceMsg(msg);
            case EVENT -> processEvent(msg);
            default -> null;
        };
        if (result != null) {
            result.setToUserName(msg.getFromUserName());
            result.setFromUserName(msg.getToUserName());
            result.setCreateTime(String.valueOf(System.currentTimeMillis()));
        }
        return result;
    }

    /**
     * 事件消息处理入口
     *
     * @param msg
     * @return
     */
    default WxMpXmlMessage processEvent(WxMpXmlMessage msg) {
        return switch (WxMpConstants.BasicEventMsgType.valueOf(msg.getEvent().toUpperCase())) {
            case SUBSCRIBE -> processSubscribeEvent(msg);
            case UNSUBSCRIBE -> processUnsubscribeEvent(msg);
            case SCAN -> processScanEvent(msg);
            case LOCATION -> processLocationEvent(msg);
            case CLICK -> processClickEvent(msg);
            case VIEW -> processViewEvent(msg);
            case TEMPLATESENDJOBFINISH -> processTemplateSendJobFinish(msg);
            default -> null;
        };

    }

    /**
     * 点击菜单跳转链接时的事件推送
     *
     * @param msg
     * @return
     */
    default WxMpXmlMessage processViewEvent(WxMpXmlMessage msg) {
        return null;
    }

    /**
     * 流程模板发送工作完成
     *
     * @param msg 味精
     * @return {@link WxMpXmlMessage}
     */
    default WxMpXmlMessage processTemplateSendJobFinish(WxMpXmlMessage msg) {
        return null;
    }


    /**
     * 自定义菜单事件
     *
     * @param msg
     * @return
     */
    default WxMpXmlMessage processClickEvent(WxMpXmlMessage msg) {
        return null;
    }


    /**
     * 上报地理位置事件
     *
     * @param msg
     * @return
     */
    default WxMpXmlMessage processLocationEvent(WxMpXmlMessage msg) {
        return null;
    }


    /**
     * 扫描带参数二维码事件
     *
     * @param msg
     * @return
     */
    default WxMpXmlMessage processScanEvent(WxMpXmlMessage msg) {
        return null;
    }


    /**
     * 取消关注事件
     *
     * @param msg
     * @return
     */
    default WxMpXmlMessage processUnsubscribeEvent(WxMpXmlMessage msg) {
        return null;
    }


    /**
     * 关注事件或者扫码关注事件，具体判断是否包含 eventKey
     *
     * @param msg
     * @return
     */
    default WxMpXmlMessage processSubscribeEvent(WxMpXmlMessage msg) {
        return null;
    }


    /**
     * 接收普通消息
     *
     * @param mp 微信推送过来的消息
     * @return 返回给微信的消息
     */
    default WxMpXmlMessage processTextMsg(WxMpXmlMessage mp) {
        return null;
    }

    /**
     * 接收图片消息
     *
     * @param mp 微信推送过来的消息
     * @return 返回给微信的消息
     */
    default WxMpXmlMessage processImageMsg(WxMpXmlMessage mp) {
        return null;
    }

    /**
     * 接收语音消息
     *
     * @param mp 微信推送过来的消息
     * @return 返回给微信的消息
     */
    default WxMpXmlMessage processVoiceMsg(WxMpXmlMessage mp) {
        return null;
    }

    /**
     * 接收视频消息
     *
     * @param mp 微信推送过来的消息
     * @return 返回给微信的消息
     */
    default WxMpXmlMessage processVideoMsg(WxMpXmlMessage mp) {
        return null;
    }

    /**
     * 接收地理位置消息
     *
     * @param mp 微信推送过来的消息
     * @return 返回给微信的消息
     */
    default WxMpXmlMessage processLocationMsg(WxMpXmlMessage mp) {
        return null;
    }

    /**
     * 接收链接消息
     *
     * @param mp 微信推送过来的消息
     * @return 返回给微信的消息
     */
    default WxMpXmlMessage processLinkMsg(WxMpXmlMessage mp) {
        return null;
    }

    /**
     * 接收小视频消息
     *
     * @param mp 微信推送过来的消息
     * @return 返回给微信的消息
     */
    default WxMpXmlMessage processShortVideoMsg(WxMpXmlMessage mp) {
        return null;
    }
}
