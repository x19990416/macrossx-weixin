/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.api.impl;

import com.google.gson.Gson;
import com.xyuxin.weixin.common.bean.WxAccessToken;
import com.xyuxin.weixin.common.utils.http.BaseHttpGetExecutor;
import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.mp.WxMpConfig;
import com.xyuxin.weixin.mp.api.WxMpService;
import com.xyuxin.weixin.mp.bean.WxJsApiTicket;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * @author Guo Limin
 */
@Slf4j
@Getter
public class BaseWxMpServiceImpl implements WxMpService {
    private final WxMpConfig config;
    private static WxAccessToken wxAccessToken;
    private final HttpClientExecutor executorFactory;

    private static WxJsApiTicket wxJsApiTicket;


    public BaseWxMpServiceImpl(WxMpConfig config, HttpClientExecutor executorFactory) {
        this.config = config;
        this.executorFactory = executorFactory;
    }

    protected synchronized WxAccessToken getWxAccessToken() throws IOException {
        String url = String.format(WxMpService.GET_ACCESS_TOKEN_URL, config.getAppId(), config.getAppSecret());
        log.info("GET_ACCESS_TOKEN_URL: {}", url);
        BaseHttpGetExecutor<String> executor = executorFactory.httpGetStringExecutor();
        WxAccessToken result = new Gson().fromJson(executor.execute(url, null), WxAccessToken.class);
        log.info("{}", result);
        return result;
    }


    protected synchronized WxJsApiTicket getWxJsApiTicket() throws IOException {
        String url = String.format(WxMpService.GET_JSAPI_TICKET, getAccessToken());
        log.info("GET_JSAPI_TICKET: {}", url);
        BaseHttpPostExecutor<String> executor = getExecutorFactory().httpPostStringExecutor();
        WxJsApiTicket result = new Gson().fromJson(executor.execute(url, null), WxJsApiTicket.class);
        if (result.getErrCode() != 0) {
            log.error("fetch wxJsApiTicket error for code is [{}]. message is [{}].", result.getErrCode(), result.getErrMsg());
        }
        return result;
    }

    @Override
    public String getJsApiTicket() throws IOException {
        if (wxJsApiTicket == null || wxJsApiTicket.isOutExpire()) {
            wxJsApiTicket = getWxJsApiTicket();
        }
        return wxJsApiTicket.getTicket();
    }

    @Override
    public String getAccessToken() throws IOException {
        if (wxAccessToken == null || wxAccessToken.isOutExpire()) {
            wxAccessToken = getWxAccessToken();
        }
        return wxAccessToken.getAccessToken();
    }
}
