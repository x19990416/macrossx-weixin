/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.bean.menu;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Guo Limin
 */
@Data
public class WxMpSelfMenuInfo  implements Serializable {
    @SerializedName("button")
    private Collection<WxMpSelfMenuButton> buttons;

    @Data
    public static class WxMpSelfMenuButton implements Serializable {
        @SerializedName("type")
        private String type;

        @SerializedName("name")
        private String name;
        @SerializedName("key")
        private String key;
        @SerializedName("url")
        private String url;
        @SerializedName("value")
        private String value;
        @SerializedName("appid")
        private String appId;
        @SerializedName("pagepath")
        private String pagePath;
        @SerializedName("sub_button")
        private SubButtons subButtons;
        @SerializedName("news_info")
        private NewsInfo newsInfo;

        @Data
        public static class SubButtons implements Serializable {

            @SerializedName("list")
            private Collection<WxMpSelfMenuButton> subButtons = new ArrayList<>();

        }

        @Data
        public static class NewsInfo implements Serializable {
            @SerializedName("list")
            private Collection<NewsInButton> news = new ArrayList<>();
        }

        @Data
        public static class NewsInButton implements Serializable {
            @SerializedName("title")
            private String title;
            @SerializedName("digest")
            private String digest;
            @SerializedName("author")
            private String author;
            @SerializedName("show_cover")
            private Integer showCover;
            @SerializedName("cover_url")
            private String coverUrl;
            @SerializedName("content_url")
            private String contentUrl;
            @SerializedName("source_url")
            private String sourceUrl;

        }

    }
}
