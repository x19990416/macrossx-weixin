/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.api;

import com.xyuxin.weixin.mp.WxMpConstants;
import com.xyuxin.weixin.mp.bean.account.WxParametricQrCode;

import java.io.IOException;

/**
 * 账号管理
 *
 * @author Guo Limin
 * @see <a href="https://developers.weixin.qq.com/doc/offiaccount/Account_Management/Generating_a_Parametric_QR_Code.html">账号管理</a>
 */
public interface WxMpAccountService {

    /**
     * 生成带参数二维码
     *
     * @See <a href="https://developers.weixin.qq.com/doc/offiaccount/Account_Management/Generating_a_Parametric_QR_Code.html">创建菜单</a>
     */
    String POST_USER_INFO = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=%s";

    /**
     * 生成带参数的二维码
     * @param actionName
     * @param expireSeconds
     * @param sceneId
     * @param sceneStr
     * @return
     * @throws IOException
     */
    public WxParametricQrCode generateParametricQrCode(WxMpConstants.QrCodeActionName actionName, Long expireSeconds, Long sceneId, String sceneStr) throws IOException;

}
