package com.xyuxin.weixin.mp.bean.user;

import com.google.gson.annotations.SerializedName;
import com.xyuxin.weixin.common.bean.WxErr;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * wx议员用户列表
 *
 * @author 50014792@qq.com
 * @date 2023/06/20
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class WxMpUserList extends WxErr implements Serializable {
    /**
     * 关注该公众账号的总用户数
     */
    private Integer total;

    /**
     * 拉取的OPENID个数，最大值为10000
     */
    private Integer count;

    /**
     * 列表数据，OPENID的列表
     */
    private MxMpUserOpenId data;
    /**
     * 拉取列表的最后一个用户的OPENID
     */
    @SerializedName("next_openid")
    private String nextOpenId;

}
