/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.api.impl;

import com.xyuxin.weixin.common.utils.http.BaseHttpGetExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.mp.WxMpConfig;
import com.xyuxin.weixin.mp.api.WxMpUserService;
import com.xyuxin.weixin.mp.bean.user.WxMpUserInfo;
import com.xyuxin.weixin.mp.bean.user.WxMpUserList;
import com.xyuxin.weixin.mp.bean.util.gson.GsonInitializer;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * @author Guo Limin
 */
public class WxMpUserServiceImpl extends BaseWxMpServiceImpl implements WxMpUserService {
    public WxMpUserServiceImpl(WxMpConfig config, HttpClientExecutor executorFactory) {
        super(config, executorFactory);
    }

    @Override
    public WxMpUserInfo fetchUserInfo(String openid, String lang) throws IOException {
        String url = String.format(WxMpUserService.GET_USER_INFO, super.getAccessToken(), openid);
        if (!StringUtils.isEmpty(lang)) {
            url = url + "&lang=zh_CN";
        }
        BaseHttpGetExecutor<String> executor = super.getExecutorFactory().httpGetStringExecutor();
        return GsonInitializer.getInstance().fromJson(executor.execute(url, null), WxMpUserInfo.class);
    }

    @Override
    public WxMpUserList fetchUserList(String nextOpenId) throws IOException {
        String url = String.format(WxMpUserService.GET_USER_LIST, super.getAccessToken());
        if (!StringUtils.isEmpty(nextOpenId)) {
            url = url + "&next_openid=" + nextOpenId;
        }
        BaseHttpGetExecutor<String> executor = super.getExecutorFactory().httpGetStringExecutor();

        return GsonInitializer.getInstance().fromJson(executor.execute(url, null), WxMpUserList.class);
    }
}
