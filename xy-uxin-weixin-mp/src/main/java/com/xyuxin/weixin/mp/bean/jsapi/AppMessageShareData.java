package com.xyuxin.weixin.mp.bean.jsapi;

import lombok.Data;
import lombok.ToString;

/**
 * 分享
 * @author guolimin
 */
@Data
@ToString
public class AppMessageShareData {
    private String title;
    private String desc;
    private String link;
    private String imgUrl;

}
