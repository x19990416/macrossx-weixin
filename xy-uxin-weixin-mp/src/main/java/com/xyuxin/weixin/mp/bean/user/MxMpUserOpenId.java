package com.xyuxin.weixin.mp.bean.user;

import lombok.Data;

import java.util.List;

/**
 * 微信用户OPENID列表
 *
 * @author 50014792@qq.com
 * @date 2023/06/20
 */
@Data
public class MxMpUserOpenId {
    private List<String> openid;
}
