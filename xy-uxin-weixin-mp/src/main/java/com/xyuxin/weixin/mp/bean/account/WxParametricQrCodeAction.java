/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.bean.account;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author Guo Limin
 */
@Accessors(chain = true)
@Data
public class WxParametricQrCodeAction implements Serializable {
    @SerializedName("expire_seconds")
    private Long expireSeconds;
    @SerializedName("action_name")
    private String actionName;
    @SerializedName("action_info")
    private ActionInfo actionInfo;

    @Data
    @Accessors(chain = true)
    public static class ActionInfo {
        @SerializedName("scene")
        private Scene scene;

    }

    @Data
    @Accessors(chain = true)
    public static class Scene {
        @SerializedName("scene_id")
        private Long sceneId;
        @SerializedName("scene_str")
        private String sceneStr;
    }
}
