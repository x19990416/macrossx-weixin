/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.api;

import com.xyuxin.weixin.mp.bean.user.WxMpUserInfo;
import com.xyuxin.weixin.mp.bean.user.WxMpUserList;

import java.io.IOException;

/**
 * 用户管理
 *
 * @author Guo Limin
 * @see <a href="https://developers.weixin.qq.com/doc/offiaccount/User_Management/User_Tag_Management.html">用户管理</a>
 */
public interface WxMpUserService {

    /**
     * 获取用户基本信息（包括UnionID机制）
     *
     * @See <a href="https://https://developers.weixin.qq.com/doc/offiaccount/User_Management/Get_users_basic_information_UnionID.html#UinonId">创建菜单</a>
     */
    String GET_USER_INFO = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s";

    /**
     * 获取用户信息
     *
     * @param openid 微信唯一标识
     * @param lang   语种(可选)
     * @return
     * @throws IOException
     */
    WxMpUserInfo fetchUserInfo(String openid, String lang) throws IOException;

    /**
     * 公众号可通过本接口来获取帐号的关注者列表，关注者列表由一串OpenID（加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。一次拉取调用最多拉取10000个关注者的OpenID，可以通过多次拉取的方式来满足需求。
     */
    String GET_USER_LIST= "https://api.weixin.qq.com/cgi-bin/user/get?access_token=%s";


    /**
     * 获取用户OpenID列表
     *
     * @param nextOpenId 第一个拉取的OPENID，不填默认从头开始拉取
     * @return {@link WxMpUserList}
     * @throws IOException ioexception
     */
    WxMpUserList fetchUserList(String nextOpenId) throws  IOException;



}
