/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.bean.menu;

import com.xyuxin.weixin.common.bean.menu.WxMenuButton;

import com.xyuxin.weixin.common.bean.menu.WxMenuRule;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Guo Limin
 */
@Data
public class WxMpConditionalMenu implements Serializable {
    private static final long serialVersionUID = -5794350513426702252L;

    @SerializedName("menu")
    private WxConditionalMenu menu;

    @SerializedName("conditionalmenu")
    private Collection<WxConditionalMenu> conditionalMenu;

    @Data
    public static class WxConditionalMenu implements Serializable {
        private static final long serialVersionUID = -2279946921755382289L;
        @SerializedName("button")
        private Collection<WxMenuButton> buttons;
        @SerializedName("matchrule")
        private WxMenuRule rule;
        @SerializedName("menuid")
        private String menuId;
    }
}
