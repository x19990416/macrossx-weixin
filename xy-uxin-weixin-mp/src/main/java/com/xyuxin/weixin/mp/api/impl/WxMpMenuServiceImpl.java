/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.api.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.xyuxin.weixin.common.bean.WxErr;
import com.xyuxin.weixin.common.bean.menu.WxMenu;
import com.xyuxin.weixin.common.utils.http.BaseHttpGetExecutor;
import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.mp.WxMpConfig;
import com.xyuxin.weixin.mp.api.WxMpMenuService;
import com.xyuxin.weixin.mp.bean.menu.WxMpConditionalMenu;
import com.xyuxin.weixin.mp.bean.menu.WxMpSelfMenuInfoResult;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.IOException;

/**
 * @author Guo Limin
 */
@Slf4j
public class WxMpMenuServiceImpl extends BaseWxMpServiceImpl implements WxMpMenuService {
    public WxMpMenuServiceImpl(WxMpConfig config, HttpClientExecutor executorFactory) {
        super(config, executorFactory);
    }

    @Override
    public boolean createMenu(@NotNull WxMenu menu) throws IOException {
        String url = String.format(WxMpMenuService.POST_CREATE_MENU_URL, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        WxErr err = new Gson().fromJson(executor.execute(url,
                        new GsonBuilder().disableHtmlEscaping().create().toJson(menu)),
                WxErr.class);
        boolean result = err.getErrCode() == 0;
        if (!result) {
            log.info("create menu error for {}", err.getErrMsg());
        }
        return result;
    }

    @Override
    public WxMpSelfMenuInfoResult queryMenuInfo() throws IOException {
        String url = String.format(WxMpMenuService.GET_QUERY_MENU_URL, super.getAccessToken());
        BaseHttpGetExecutor<String> executor = super.getExecutorFactory().httpGetStringExecutor();
        return new Gson().fromJson(executor.execute(url, null), WxMpSelfMenuInfoResult.class);
    }

    @Override
    public boolean deleteMenu() throws IOException {
        String url = String.format(WxMpMenuService.GET_DEL_MENU_URL, super.getAccessToken());
        BaseHttpGetExecutor<String> executor = super.getExecutorFactory().httpGetStringExecutor();
        WxErr err = new Gson().fromJson(executor.execute(url, null), WxErr.class);
        boolean result = err.getErrCode() == 0;
        if (!result) {
            log.info("create menu error for {}", err.getErrMsg());
        }
        return result;
    }

    @Override
    public boolean createConditionalMenu(WxMenu menu) throws IOException {
        String url = String.format(WxMpMenuService.POST_CREATE_CONDITIONAL_MENU_URL, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        WxErr err = new Gson().fromJson(executor.execute(url,
                new GsonBuilder().disableHtmlEscaping().create().toJson(menu)), WxErr.class);
        boolean result = err.getErrCode() == 0;
        if (!result) {
            log.info("create conditional menu error for {}", err.getErrMsg());
        }
        return result;
    }

    @Override
    public WxMpConditionalMenu getMenu() throws IOException {
        String url = String.format(WxMpMenuService.GET_MENU_URL, super.getAccessToken());
        BaseHttpGetExecutor<String> executor = super.getExecutorFactory().httpGetStringExecutor();
        return new Gson().fromJson(executor.execute(url, null), WxMpConditionalMenu.class);
    }
}
