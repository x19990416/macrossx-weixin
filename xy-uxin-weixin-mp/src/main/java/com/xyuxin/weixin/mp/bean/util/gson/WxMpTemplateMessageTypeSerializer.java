package com.xyuxin.weixin.mp.bean.util.gson;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.xyuxin.weixin.mp.bean.template.WxMpTemplateData;
import com.xyuxin.weixin.mp.bean.template.WxMpTemplateMessage;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Type;

/**
 * 微信小细木板序列化适配器
 *
 * @author 50014792@qq.com
 * @date 2023/06/27
 */
public class WxMpTemplateMessageTypeSerializer implements JsonSerializer<WxMpTemplateMessage> {

    @Override
    public JsonElement serialize(WxMpTemplateMessage message, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject messageJson = new JsonObject();
        messageJson.addProperty("touser", message.getToUser());
        messageJson.addProperty("template_id", message.getTemplateId());
        if (StringUtils.isNotBlank(message.getClientMsgId())) {
            messageJson.addProperty("client_msg_id", message.getClientMsgId());
        }
        if (message.getUrl() != null) {
            messageJson.addProperty("url", message.getUrl());
        }

        final WxMpTemplateMessage.MiniProgram miniProgram = message.getMiniProgram();
        if (miniProgram != null) {
            JsonObject miniProgramJson = new JsonObject();
            miniProgramJson.addProperty("appid", miniProgram.getAppid());
            if (miniProgram.isUsePath()) {
                miniProgramJson.addProperty("path", miniProgram.getPagePath());
            } else {
                miniProgramJson.addProperty("pagepath", miniProgram.getPagePath());
            }
            messageJson.add("miniprogram", miniProgramJson);
        }

        JsonObject data = new JsonObject();
        messageJson.add("data", data);

        for (WxMpTemplateData datum : message.getData()) {
            JsonObject dataJson = new JsonObject();
            dataJson.addProperty("value", datum.getValue());
            if (datum.getColor() != null) {
                dataJson.addProperty("color", datum.getColor());
            }
            data.add(datum.getName(), dataJson);
        }

        return messageJson;
    }


}
