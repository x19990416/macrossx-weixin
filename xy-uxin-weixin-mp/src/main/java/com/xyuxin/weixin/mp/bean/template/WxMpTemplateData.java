package com.xyuxin.weixin.mp.bean.template;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 微信消息模板内容
 *
 * @author 50014792@qq.com
 * @date 2023/06/27
 */
@Data
@Accessors(chain = true)
public class WxMpTemplateData implements Serializable {
    private String name;
    private String value;
    /**
     * 删除
     * @see https://developers.weixin.qq.com/community/develop/doc/000a2ae286cdc0f41a8face4c51801?page=3
     */
    @Deprecated
    private String color;
}
