/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.bean.util.gson;

import com.xyuxin.weixin.mp.WxMpConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.xyuxin.weixin.mp.bean.template.WxMpTemplateMessage;

/**
 * @author Guo Limin
 */
public class GsonInitializer {
    private static Gson instance = null;

    public static Gson getInstance() {
        if (instance == null) {
            instance = new GsonBuilder()
                    .registerTypeAdapter(WxMpConstants.SubscribeSceneType.class, new SubscribeSceneTypeSerializer())
                    .registerTypeAdapter(WxMpTemplateMessage.class, new WxMpTemplateMessageTypeSerializer())
                    .create();
        }
        return instance;
    }

}
