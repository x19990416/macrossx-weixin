/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.bean.account;

import com.xyuxin.weixin.common.bean.WxErr;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author Guo Limin
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class WxParametricQrCode extends WxErr {
    @SerializedName("ticket")
    private String ticker;
    @SerializedName("expire_seconds")
    private long expireSeconds;
    @SerializedName("url")
    private String url;

}

