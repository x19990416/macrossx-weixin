package com.xyuxin.weixin.mp.api.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.mp.WxMpConfig;
import com.xyuxin.weixin.mp.api.WxMpTemplateMessageService;
import com.xyuxin.weixin.mp.bean.template.WxMpTemplateMessage;
import com.xyuxin.weixin.mp.bean.util.gson.GsonInitializer;

import java.io.IOException;

/**
 * 微信消息模板服务实现
 *
 * @author 50014792@qq.com
 * @date 2023/06/27
 */
public class WxMpTemplateMessageServiceImpl extends BaseWxMpServiceImpl implements WxMpTemplateMessageService {
    public WxMpTemplateMessageServiceImpl(WxMpConfig config, HttpClientExecutor executorFactory) {
        super(config, executorFactory);
    }

    @Override
    public String sendTemplateMessage(WxMpTemplateMessage templateMessage) throws IOException {
        String url = String.format(WxMpTemplateMessageService.POST_TEMPLATE_SEND, super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        String content = executor.execute(url, GsonInitializer.getInstance().toJson(templateMessage));
        JsonObject jsonObj = JsonParser.parseString(content).getAsJsonObject();
        if (jsonObj.get("errcode").getAsInt() == 0) {
            return jsonObj.get("msgid").getAsString();
        }
        throw new RuntimeException(content);
    }
}
