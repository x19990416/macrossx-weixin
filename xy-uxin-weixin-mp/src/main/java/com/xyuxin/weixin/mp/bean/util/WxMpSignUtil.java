/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.mp.bean.util;

import com.xyuxin.weixin.common.utils.SignUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import javax.validation.constraints.NotEmpty;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Guo Limin
 */
public class WxMpSignUtil extends SignUtil {
    public static String wxJsApiTicketSign(@NotEmpty String jsApiTicket, @NotEmpty String noneStr, @NotEmpty String timestamp, @NotEmpty String targetUrl) throws NoSuchAlgorithmException {
        Map<String, String> params = new HashMap<>(4);
        params.put(SignParam.JSAPI.value(), jsApiTicket);
        params.put(SignParam.NonceStr.value(), noneStr);
        params.put(SignParam.TimeStamp.value(), timestamp);
        params.put(SignParam.Url.value(), targetUrl);
        return sign(params);
    }

    private enum SignParam {
        JSAPI("jsapi_ticket"),
        NonceStr("noncestr"),
        TimeStamp("timestamp"),
        Url("url");

        private String value;

        SignParam(String value) {
            this.value = value;
        }

        public String value() {
            return this.value;
        }
    }
}
