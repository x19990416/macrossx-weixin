package com.xyuxin.weixin.chatbot.api.impl;

import com.google.gson.Gson;
import com.xyuxin.weixin.chatbot.WxChatbotConfig;
import com.xyuxin.weixin.chatbot.api.WxChatbotService;
import com.xyuxin.weixin.common.bean.WxAccessToken;
import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
@Slf4j
@Getter
public class BaseWxChatbotServiceImpl implements WxChatbotService {
    private final WxChatbotConfig config;
    private static WxAccessToken wxAccessToken;
    private final HttpClientExecutor executorFactory;


    public BaseWxChatbotServiceImpl(WxChatbotConfig config, HttpClientExecutor executorFactory) {
        this.config = config;
        this.executorFactory = executorFactory;
    }

    protected synchronized WxAccessToken getWxAccessToken() throws IOException {
        String url = String.format(WxChatbotService.POST_ACCESS_TOKEN_URL, config.getToken());
        log.info("GET_ACCESS_TOKEN_URL: {}", url);
        BaseHttpPostExecutor<String> executor = executorFactory.httpPostStringExecutor();
        String resp = executor.execute(url, new Gson().toJson(config.getUserInfo()));
        WxAccessToken result = new Gson().fromJson(resp,
                WxAccessToken.class);
        log.info("{}", result);
        return result;
    }


    @Override
    public String getAccessToken() throws IOException {
        if (wxAccessToken == null || wxAccessToken.isOutExpire()) {
            wxAccessToken = getWxAccessToken();
        }
        return wxAccessToken.getAccessToken();
    }
}
