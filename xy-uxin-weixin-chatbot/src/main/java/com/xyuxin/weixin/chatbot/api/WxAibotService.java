package com.xyuxin.weixin.chatbot.api;

import com.xyuxin.weixin.chatbot.bean.aibot.AiBotRequest;
import com.xyuxin.weixin.chatbot.bean.aibot.AiBotResponse;

import java.io.IOException;

public interface WxAibotService {

    //https://chatbot.weixin.qq.com/openapi/aibot/{TOKEN}
    /**
     * 智能对话接口
     * https://developers.weixin.qq.com/doc/aispeech/confapi/INTERFACEDOCUMENT.html
     */
    String POST_OPENAPI_AIBOT = "https://chatbot.weixin.qq.com/openapi/aibot/%s";


    AiBotResponse chat(AiBotRequest request) throws IOException;

}
