package com.xyuxin.weixin.chatbot.bean.aibot;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@ToString
@Accessors(chain = true)
public class AiBotRequest {
    private String signature;
    /**
     * 用户发送的消息
     */
    private String query;
    /**
     *  	默认是online, debug是测试环境,online是线上环境
     */
    private String env;

    /**
     * 限定技能命中范围 比如：["技能1"]，只匹配命中“技能1”中的所有问答内容
     */
    @SerializedName("first_priority_skills")
    private List<String> firstPrioritySkills;
    /**
     * 限定技能命中范围 比如：["技能2"]，只匹配命中“技能2”中的所有问答内容,比first_priority_skills命中优先级低
     */
    @SerializedName("second_priority_skills")
    private List<String> secondPrioritySkills;
}
