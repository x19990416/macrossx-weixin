package com.xyuxin.weixin.chatbot.bean;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class WxChatbotSign {
    private String username;
    private String avatar;
    @SerializedName("userid")
    private String userId;
}
