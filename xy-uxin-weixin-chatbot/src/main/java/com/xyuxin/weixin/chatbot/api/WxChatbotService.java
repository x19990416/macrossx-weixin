package com.xyuxin.weixin.chatbot.api;

import com.xyuxin.weixin.common.service.WxService;

public interface WxChatbotService extends WxService {

    /**
     * 获取signature接口
     */
    String POST_ACCESS_TOKEN_URL = "https://chatbot.weixin.qq.com/openapi/sign/%s";
}
