package com.xyuxin.weixin.chatbot.bean.aibot;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class AiBotResponse {
    private Integer ansNodeId;
    private String ansNodeName;
    private String title;
    private String answer;
    @SerializedName("answer_type")
    private String  answerType;
    private List<Object> msg;
    private String confidence;
    private List<Option> options;
    @SerializedName("from_user_name")
    private String fromUserName;
    @SerializedName("to_user_name")
    private String toUserName;
    private String status;
    private String msgId;
    private String query;
    private Long createTime;
    @Data
    public static class Option{
        private String ansNodeName;
        private String title;
        private String answer;
        private String confidence;
    }
}
