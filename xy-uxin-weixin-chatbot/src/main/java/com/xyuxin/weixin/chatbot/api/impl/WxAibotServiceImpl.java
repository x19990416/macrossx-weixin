package com.xyuxin.weixin.chatbot.api.impl;

import com.google.gson.Gson;
import com.xyuxin.weixin.chatbot.WxChatbotConfig;
import com.xyuxin.weixin.chatbot.api.WxAibotService;
import com.xyuxin.weixin.chatbot.bean.WxChatbotSign;
import com.xyuxin.weixin.chatbot.bean.aibot.AiBotRequest;
import com.xyuxin.weixin.chatbot.bean.aibot.AiBotResponse;
import com.xyuxin.weixin.common.bean.WxAccessToken;
import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import com.xyuxin.weixin.common.utils.http.apache.ApacheHttpClientExecutor;

import java.io.IOException;

public class WxAibotServiceImpl  extends BaseWxChatbotServiceImpl implements WxAibotService {
    public WxAibotServiceImpl(WxChatbotConfig config, HttpClientExecutor executorFactory) {
        super(config, executorFactory);
    }

    @Override
    public AiBotResponse chat(AiBotRequest request) throws IOException {
        String url = String.format(POST_OPENAPI_AIBOT,super.getConfig().getToken());
        request.setSignature(super.getAccessToken());
        BaseHttpPostExecutor<String> executor = super.getExecutorFactory().httpPostStringExecutor();
        String resp = executor.execute(url, new Gson().toJson(request));
        AiBotResponse result = new Gson().fromJson(resp,
                AiBotResponse.class);

        return result;
    }

    public static void main(String...s) throws IOException {
        WxChatbotConfig cfg = new WxChatbotConfig();
        cfg.setAppId("O0dDL3h1WxpdFlt").setToken("115PkZLZlARbmTFDpFMVqAnDXwc2JI").setEncodingAESKey(
                "Uch7jBzkWzHjKi4vIbtGcymS3uEH0XVGtYux8sfvC3h").setUserInfo(new WxChatbotSign().setUserId("macrossx"));
        AiBotRequest requ = new AiBotRequest();
        requ.setQuery("你好");
        System.out.println(new WxAibotServiceImpl(cfg,new ApacheHttpClientExecutor()).chat(requ).getAnswer());
    }
}


