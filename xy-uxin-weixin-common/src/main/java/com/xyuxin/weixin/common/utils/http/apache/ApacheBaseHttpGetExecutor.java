/*
 * Copyright (c) 2021, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.common.utils.http.apache;

import com.xyuxin.weixin.common.utils.http.BaseHttpGetExecutor;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;

/**
 * 基于apache httpclient 的 get 执行器
 *
 * @author Guo Limin
 */
public class ApacheBaseHttpGetExecutor<T> extends BaseHttpGetExecutor<T> {

    private CloseableHttpClient httpClient;

    private ResponseHandler<T> handler;

    private final static String QUESTION_MARK = "?";

    public ApacheBaseHttpGetExecutor(@NonNull CloseableHttpClient httpClient, @NonNull ResponseHandler<T> handler) {
        this.httpClient = httpClient;
        this.handler = handler;
    }

    @Override
    public T execute(String uri, String data) throws IOException {
        if (!StringUtils.isEmpty(data)) {
            if (uri.indexOf(QUESTION_MARK) == -1) {
                uri += QUESTION_MARK;
            }
            uri += uri.endsWith(QUESTION_MARK) ? data : '&' + data;
        }
        HttpGet httpGet = new HttpGet(uri);
        try (CloseableHttpResponse response = this.httpClient.execute(httpGet)) {
            return handler.handleResponse(response);
        } finally {
            httpGet.releaseConnection();
        }

    }
}
