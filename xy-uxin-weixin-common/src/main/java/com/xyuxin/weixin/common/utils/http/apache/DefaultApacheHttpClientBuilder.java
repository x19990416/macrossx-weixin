/*
 * Copyright (c) 2021, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.common.utils.http.apache;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

/**
 * http client 构造器，提供默认配置
 *
 * @author Guo Limin
 */
@Data
@Slf4j
public class DefaultApacheHttpClientBuilder implements ApacheHttpClientBuilder {
    private SSLConnectionSocketFactory sslConnectionSocketFactory = SSLConnectionSocketFactory.getSocketFactory();
    private final PlainConnectionSocketFactory plainConnectionSocketFactory = PlainConnectionSocketFactory.getSocketFactory();
    private int connectionRequestTimeout = -1;
    private int connectionTimeout = 5000;
    private int socketTimeout = 5000;
    private int idleConnTimeout = 60000;
    private int checkWaitTime = 60000;
    private int maxConnPerHost = 10;
    private int maxTotalConn = 50;
    private String userAgent;
    private CloseableHttpClient closeableHttpClient;

    @Override
    public ApacheHttpClientBuilder sslConnectionSocketFactory(SSLConnectionSocketFactory sslConnectionSocketFactory) {
        this.sslConnectionSocketFactory = sslConnectionSocketFactory;
        return this;
    }

    private SSLConnectionSocketFactory buildSslConnectionSocketFactory() {
        try {
            SSLContext sslcontext = SSLContexts.custom()
                    //忽略掉对服务器端证书的校验
                    .loadTrustMaterial((TrustStrategy) (chain, authType) -> true).build();

            return new SSLConnectionSocketFactory(
                    sslcontext,
                    new String[]{"TLSv1"},
                    null,
                    SSLConnectionSocketFactory.getDefaultHostnameVerifier());
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            log.error("构建SSL连接工厂时发生异常！", e);
        }

        return null;
    }

    private final HttpRequestRetryHandler httpRequestRetryHandler = (exception, executionCount, context) -> false;

    @Override
    public synchronized CloseableHttpClient build() {
        Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", this.plainConnectionSocketFactory)
                .register("https", this.sslConnectionSocketFactory)
                .build();


        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);
        connectionManager.setMaxTotal(this.maxTotalConn);
        connectionManager.setDefaultMaxPerRoute(this.maxConnPerHost);
        connectionManager.setDefaultSocketConfig(
                SocketConfig.copy(SocketConfig.DEFAULT)
                        .setSoTimeout(this.socketTimeout)
                        .build());


        HttpClientBuilder httpClientBuilder = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true)
                .setSSLSocketFactory(buildSslConnectionSocketFactory())
                .setDefaultRequestConfig(RequestConfig.custom().setSocketTimeout(socketTimeout)
                        .setConnectTimeout(connectionTimeout).setConnectionRequestTimeout(connectionRequestTimeout).build())
                .setRetryHandler(httpRequestRetryHandler);

        return httpClientBuilder.build();
    }


}
