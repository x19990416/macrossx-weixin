/*
 * Copyright (c) 2021, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.common.enums;

/**
 * 微信类型
 *
 * @author Guo Limin
 * @date 2021/12/09
 * @see <a href='https://developers.weixin.qq.com/doc/' >微信官方文档</a>
 */
public enum WxType {
    /**
     * 小程序
     *
     * @see <a href='https://developers.weixin.qq.com/miniprogram/dev/framework/'>小程序</a>
     */
    MiniApp,
    /**
     * 公众号
     *
     * @see <a href='https://developers.weixin.qq.com/doc/offiaccount/Getting_Started/Overview'>公众号</a>
     */
    MP,
    /**
     * 小商店
     */
    MiniStore,
    /**
     * 企业微信
     */
    CP,
    /**
     * 微信支付
     *
     * @see <a href='https://pay.weixin.qq.com/wiki/doc/apiv3/wxpay/pages/index.shtml'>微信支付</a>
     */
    Pay
}
