package com.xyuxin.weixin.common.utils;

import org.apache.commons.codec.Charsets;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.security.*;
import java.util.Arrays;
import java.util.Base64;

public class AESUtil {
    private final static String CIPHER_CBC_PADDING = "AES/CBC/PKCS5Padding";

    public static String generateKey() throws NoSuchAlgorithmException {
        KeyGenerator generator = KeyGenerator.getInstance("AES");
        generator.init(256);
        return Base64.getEncoder().encodeToString(generator.generateKey().getEncoded());
    }

    public static SecretKey loadSecretKey(String base64Key) {
        return new SecretKeySpec(Base64.getDecoder().decode(base64Key), "AES");
    }

    public static byte[] encrypt(byte[] source, SecretKey key) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        Cipher cipher = Cipher.getInstance(CIPHER_CBC_PADDING);
        byte[] iv = new byte[16];

        SecureRandom random = new SecureRandom();
        random.nextBytes(iv);
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));

        return cipher.doFinal(source);
    }

    public static byte[] decrypt(byte[] source, SecretKey key) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
        byte[] iv = Arrays.copyOfRange(key.getEncoded(), 0, 16);
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
        return cipher.doFinal(source);
    }
//    public static byte[] decrypt32(byte[] source, SecretKey key) throws NoSuchPaddingException,
//            NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
//        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
//        byte[] iv = Arrays.copyOfRange(key.getEncoded(), 0, 16);
//        System.out.println(source.length %32);
//        System.out.println(source.length);
//        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
//        return cipher.doFinal(source);
//    }

    public static String getMsg(byte[] data) {
        byte[] content = Arrays.copyOfRange(data, 16, data.length);
        int msgLen = ByteBuffer.wrap(Arrays.copyOfRange(content, 0, 4)).getInt();
        return new String(Arrays.copyOfRange(content, 4, msgLen + 4), Charsets.UTF_8);
    }

    public static String getReceiveId(byte[] data) {
        byte[] content = Arrays.copyOfRange(data, 16, data.length);
        int msgLen = ByteBuffer.wrap(Arrays.copyOfRange(content, 0, 4)).getInt();
        return new String(Arrays.copyOfRange(content, msgLen + 4, content.length), Charsets.UTF_8);
    }

}
