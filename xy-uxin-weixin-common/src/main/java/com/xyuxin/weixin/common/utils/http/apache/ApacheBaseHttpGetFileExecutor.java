package com.xyuxin.weixin.common.utils.http.apache;

import com.xyuxin.weixin.common.utils.http.BaseHttpGetExecutor;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;

public class ApacheBaseHttpGetFileExecutor<T> extends BaseHttpGetExecutor<T> {

    private CloseableHttpClient httpClient;

    private ResponseHandler<T> handler;

    private final static String QUESTION_MARK = "?";

    public ApacheBaseHttpGetFileExecutor(@NonNull CloseableHttpClient httpClient, @NonNull ResponseHandler<T> handler) {
        this.httpClient = httpClient;
        this.handler = handler;
    }

    @Override
    public T execute(String uri, String data) throws IOException {
        if (!StringUtils.isEmpty(data)) {
            if (uri.indexOf(QUESTION_MARK) == -1) {
                uri += QUESTION_MARK;
            }
            uri += uri.endsWith(QUESTION_MARK) ? data : '&' + data;
        }
        HttpGet httpGet = new HttpGet(uri);
        try (CloseableHttpResponse response = this.httpClient.execute(httpGet)) {
            return handler.handleResponse(response);
        } finally {
            httpGet.releaseConnection();
        }

    }
}