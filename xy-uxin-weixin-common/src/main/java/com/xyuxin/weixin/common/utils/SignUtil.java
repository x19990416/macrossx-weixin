/*
 * Copyright (c) 2021, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.common.utils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.constraints.NotEmpty;
import java.awt.*;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Timestamp;
import java.util.*;

/**
 * 微信签名信息
 *
 * @author Guo Limin
 */
@Slf4j
@Data
public class SignUtil {
    public static String wxSign(@NotEmpty String message, @NotEmpty String key) {
        try {
            Mac sha256 = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
            sha256.init(secretKeySpec);
            byte[] bytes = sha256.doFinal(message.getBytes(StandardCharsets.UTF_8));
            return Hex.encodeHexString(bytes).toUpperCase();
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public static String getSignTimestamp(){
        String timestamp = String.valueOf(System.currentTimeMillis());
        return timestamp.substring(0,timestamp.length()-3);
    }

    public static String sign(String...data) throws NoSuchAlgorithmException {
        Arrays.sort(data);
        StringBuilder sb = new StringBuilder();
        for (String str : data) {
            sb.append(str);
        }
        MessageDigest sha1 = MessageDigest.getInstance("SHA1");
        sha1.update(sb.toString().getBytes(StandardCharsets.UTF_8));
        return Hex.encodeHexString(sha1.digest()).toLowerCase();
    }

    public static String sign(Map<String, String> signParams) throws NoSuchAlgorithmException {
        StringBuffer sb = new StringBuffer();
        MessageDigest sha1 = MessageDigest.getInstance("SHA1");
        signParams.entrySet().stream().sorted(Map.Entry.comparingByKey(Comparator.naturalOrder())).forEach(entry -> {
            sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        });
        sha1.update(sb.substring(0,sb.length()-1).getBytes(StandardCharsets.UTF_8));
        return Hex.encodeHexString(sha1.digest());
    }

    public static boolean wxSignCheck(@NotEmpty String token, @NotEmpty String timestamp, @NotEmpty String nonce, @NotEmpty String signature) {
        try {
            String[] strArray = {token, timestamp, nonce};
            Arrays.sort(strArray);
            StringBuilder sb = new StringBuilder();
            for (String str : strArray) {
                sb.append(str);
            }
            MessageDigest sha1 = MessageDigest.getInstance("SHA1");
            sha1.update(sb.toString().getBytes(StandardCharsets.UTF_8));
            String currentSignature = Hex.encodeHexString(sha1.digest()).toLowerCase();
            return currentSignature.equals(signature);
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }
    public static boolean wxSignCheck(@NotEmpty String token, @NotEmpty String timestamp,@NotEmpty String nonce,@NotEmpty String encryptStr , @NotEmpty String signature) {
        try {
            String[] strArray = {token, timestamp, nonce,encryptStr};
            Arrays.sort(strArray);
            StringBuilder sb = new StringBuilder();
            for (String str : strArray) {
                sb.append(str);
            }
            MessageDigest sha1 = MessageDigest.getInstance("SHA1");
            sha1.update(sb.toString().getBytes(StandardCharsets.UTF_8));
            String currentSignature = Hex.encodeHexString(sha1.digest()).toLowerCase();
            return currentSignature.equals(signature);
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(), e);
        }
        return false;
    }
}
