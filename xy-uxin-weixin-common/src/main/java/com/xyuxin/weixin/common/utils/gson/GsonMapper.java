package com.xyuxin.weixin.common.utils.gson;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * @author guolimin
 */
public class GsonMapper {
    private static final Map<Class<?>, Function<JsonElement, Object>> TYPE_MAPPER = Map.of(String.class, JsonElement::getAsString, Integer.class, JsonElement::getAsInt, Long.class, JsonElement::getAsLong, BigDecimal.class, JsonElement::getAsBigDecimal, Float.class, JsonElement::getAsFloat,Boolean.class,JsonElement::getAsBoolean);

    public static JsonObject buildJsonObject(Object... keyOrValue) {
        JsonObject result = new JsonObject();
        put(result, keyOrValue);
        return result;
    }

    /**
     * 批量向JsonObject对象中添加属性
     *
     * @param jsonObject 原始JsonObject对象
     * @param keyOrValue 包含key或value的数组
     */
    public static void put(JsonObject jsonObject, Object... keyOrValue) {
        if (keyOrValue.length % 2 == 1) {
            throw new RuntimeException("参数个数必须为偶数");
        }

        for (int i = 0; i < keyOrValue.length / 2; i++) {
            final Object key = keyOrValue[2 * i];
            final Object value = keyOrValue[2 * i + 1];
            if (value == null) {
                jsonObject.add(key.toString(), null);
                continue;
            }

            if (value instanceof Boolean) {
                jsonObject.addProperty(key.toString(), (Boolean) value);
            } else if (value instanceof Character) {
                jsonObject.addProperty(key.toString(), (Character) value);
            } else if (value instanceof Number) {
                jsonObject.addProperty(key.toString(), (Number) value);
            } else if (value instanceof JsonElement) {
                jsonObject.add(key.toString(), (JsonElement) value);
            } else if (value instanceof List) {
                JsonArray array = new JsonArray();
                ((List<?>) value).forEach(a -> array.add(a.toString()));
                jsonObject.add(key.toString(), array);
            } else {
                jsonObject.add(key.toString(), new Gson().toJsonTree(value));
            }
        }

    }

    public static <T> List<T> getArray(Class<T> clazz, JsonArray array) {
        if (!GsonMapper.TYPE_MAPPER.containsKey(clazz) || array == null) {
            return null;
        }
        List<T> ret = new ArrayList<>();
        for (JsonElement element : array) {
            ret.add((T)GsonMapper.TYPE_MAPPER.get(clazz).apply(element));
        }
        return ret;
    }
    public  static <T>  T get(Class<T> tClass,JsonElement element){
        System.out.println(tClass +"\t"+element);
        if(element == null || element.isJsonNull()){
            return  null;
        }
        return (T)TYPE_MAPPER.get(tClass).apply(element);
    }
    @SneakyThrows
    public static <T> T parse(Class<T> clazz, JsonObject jsonObj) {
        T ret = clazz.getDeclaredConstructor().newInstance();
        for (Field field : clazz.getDeclaredFields()) {
            SerializedName annotation = field.getAnnotation(SerializedName.class);
            String key = annotation == null ? field.getName() : annotation.value();
            field.setAccessible(true);
            JsonElement data = jsonObj.get(key);
            if (data != null && GsonMapper.TYPE_MAPPER.containsKey(field.getType())) {
                field.set(ret, GsonMapper.TYPE_MAPPER.get(field.getType()).apply(data));
            }
            if (data != null && field.getType().equals(List.class)) {
                Type[] actualTypeArguments = ((ParameterizedType) field.getGenericType()).getActualTypeArguments();
                Class<?> type = Class.forName(actualTypeArguments[0].getTypeName());
                field.set(ret,    getArray(type,data.getAsJsonArray()));
            }
            field.setAccessible(false);
        }
        return ret;
    }
}
