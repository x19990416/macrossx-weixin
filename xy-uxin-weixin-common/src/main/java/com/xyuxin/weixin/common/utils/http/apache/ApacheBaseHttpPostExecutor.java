/*
 * Copyright (c) 2021, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.common.utils.http.apache;

import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Consts;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;

/**
 * 基于apache httpclient 的 get 执行器
 *
 * @author Guo Limin
 */
@Slf4j
public class ApacheBaseHttpPostExecutor<T> extends BaseHttpPostExecutor<T> {

    private CloseableHttpClient httpClient;

    private ResponseHandler<T> handler;

    private final static String QUESTION_MARK = "?";

    public ApacheBaseHttpPostExecutor(@NonNull CloseableHttpClient httpClient, @NonNull ResponseHandler<T> handler) {
        this.httpClient = httpClient;
        this.handler = handler;
    }

    @Override
    public T execute(String uri, String data) throws IOException {
        HttpPost httpPost = new HttpPost(uri);

        if (!StringUtils.isEmpty(data)) {
            StringEntity entity = new StringEntity(data, Consts.UTF_8);
            entity.setContentType("application/json; charset=utf-8");
            httpPost.setEntity(entity);
        }
        try (CloseableHttpResponse response = this.httpClient.execute(httpPost)) {
            return handler.handleResponse(response);
        } finally {
            httpPost.releaseConnection();
        }

    }
}
