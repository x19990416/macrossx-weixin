/*
 * Copyright (c) 2021, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.common.utils.http.apache;

import com.xyuxin.weixin.common.utils.http.BaseHttpFileExecutor;
import com.xyuxin.weixin.common.utils.http.bean.BaseFileEntity;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Consts;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * 基于apache httpclient 的 get 执行器
 *
 * @author Guo Limin
 */
@Slf4j
public class ApacheBaseHttpPostFileExecutor<T> extends BaseHttpFileExecutor<T> {

    private CloseableHttpClient httpClient;

    private ResponseHandler<T> handler;

    public ApacheBaseHttpPostFileExecutor(@NonNull CloseableHttpClient httpClient, @NonNull ResponseHandler<T> handler) {
        this.httpClient = httpClient;
        this.handler = handler;
    }

    @Override
    public T execute(String uri, Map<String, Object> data) throws IOException {
        HttpPost httpPost = new HttpPost(uri);

        if (data != null && !data.isEmpty()) {
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.setCharset(Consts.UTF_8);
            builder.setContentType(ContentType.MULTIPART_FORM_DATA);
            for (Map.Entry<String, Object> entry : data.entrySet()) {
                addMultipartBody(builder, entry.getKey(), entry.getValue());
            }
            httpPost.setEntity(builder.build());
        }
        httpPost.setHeader("Content-Type", "multipart/form-data");
        try (CloseableHttpResponse response = this.httpClient.execute(httpPost)) {
            return handler.handleResponse(response);
        } finally {
            httpPost.releaseConnection();
        }

    }

    private void addMultipartBody(MultipartEntityBuilder builder, String key, Object data) {
        if (data == null) {
            return;
        }
        if (data instanceof BaseFileEntity entity) {
            builder.addBinaryBody(key, entity.getFileStream(), ContentType.APPLICATION_OCTET_STREAM, entity.getFileName());
        } else if (data instanceof File file) {
            System.err.println(file.getName());
            builder.addBinaryBody(key, file, ContentType.DEFAULT_BINARY, file.getName());
        } else {
            builder.addTextBody(key, data.toString(), ContentType.create("text/plain", StandardCharsets.UTF_8));
        }
    }
}
