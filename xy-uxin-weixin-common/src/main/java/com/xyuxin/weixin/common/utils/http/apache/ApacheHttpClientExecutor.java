/*
 * Copyright (c) 2021, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.common.utils.http.apache;

import com.xyuxin.weixin.common.utils.http.BaseHttpFileExecutor;
import com.xyuxin.weixin.common.utils.http.BaseHttpGetExecutor;
import com.xyuxin.weixin.common.utils.http.BaseHttpPostExecutor;
import com.xyuxin.weixin.common.utils.http.HttpClientExecutor;
import org.apache.http.impl.client.CloseableHttpClient;

/**
 * 获取 apache 执行器
 *
 * @author Guo Limin
 */
public class ApacheHttpClientExecutor implements HttpClientExecutor {

    @Override
    public BaseHttpGetExecutor httpGetStringExecutor() {
        return new ApacheBaseHttpGetExecutor(getClient(), new StringResponseHandler());
    }

    @Override
    public BaseHttpPostExecutor httpPostStringExecutor() {
        return new ApacheBaseHttpPostExecutor(getClient(), new StringResponseHandler());
    }

    @Override
    public BaseHttpFileExecutor httpPostFileExecutor() {
        return new ApacheBaseHttpPostFileExecutor(getClient(), new StringResponseHandler());
    }

    @Override
    public BaseHttpFileExecutor httpGetFileExecutor() {
        return new ApacheBaseHttpPostFileExecutor(getClient(), new InputStreamResponseHandler());
    }

    public CloseableHttpClient getClient() {
        return new DefaultApacheHttpClientBuilder().build();
    }


}
