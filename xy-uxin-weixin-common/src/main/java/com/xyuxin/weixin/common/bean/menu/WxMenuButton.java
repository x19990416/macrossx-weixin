/*
 * Copyright (c) 2022, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.common.bean.menu;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Guo Limin
 * @see <a href="https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Creating_Custom-Defined_Menu.html">创建菜单接口</a>
 */
@Data
@Accessors(chain = true)
public class WxMenuButton implements Serializable {
    private static final long serialVersionUID = -1070939403109776555L;

    /**
     * 菜单的响应动作类型.
     * view表示网页类型，
     * click表示点击类型，
     * miniprogram表示小程序类型
     */
    private String type;

    /**
     * 菜单标题，不超过16个字节，子菜单不超过60个字节.
     */
    private String name;

    /**
     * 菜单KEY值，用于消息接口推送，不超过128字节.
     * click等点击类型必须
     */
    private String key;

    /**
     * 网页链接.<br/>
     * 用户点击菜单可打开链接，不超过1024字节。type为miniprogram时，不支持小程序的老版本客户端将打开本url。
     * view、miniprogram类型必须
     */
    private String url;

    /**
     * 调用新增永久素材接口返回的合法media_id.<br>
     * media_id类型和view_limited类型必须
     */
    @SerializedName("media_id")
    private String mediaId;

    /**
     * 小程序的appid.
     * miniprogram类型必须
     */
    @SerializedName("appid")
    private String appId;

    /**
     * 小程序的页面路径.
     * miniprogram类型必须
     */
    @SerializedName("pagepath")
    private String pagePath;

    @SerializedName("sub_button")
    private Collection<WxMenuButton> subButtons;

}

