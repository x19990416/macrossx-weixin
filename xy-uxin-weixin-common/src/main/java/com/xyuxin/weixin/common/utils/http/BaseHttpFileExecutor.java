/*
 * Copyright (c) 2021, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.common.utils.http;

import java.io.IOException;
import java.util.Map;

/**
 * 执行 HTTP POST
 * @author Guo Limin
 */
public abstract class BaseHttpFileExecutor<T> implements RequestExecutor<T, Map<String,Object>>{

    @Override
    public void execute(String uri, Map<String,Object> data, ResponseHandler<T> handler) throws IOException {
        handler.handle(this.execute(uri,data));
    }

}
