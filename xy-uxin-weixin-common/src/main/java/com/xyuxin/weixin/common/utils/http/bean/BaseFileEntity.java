package com.xyuxin.weixin.common.utils.http.bean;

import lombok.Data;

import java.io.InputStream;
/**
 * @author guolimin
 */
@Data
public class BaseFileEntity {
    private InputStream fileStream;
    private String fileName;
}
