/*
 * Copyright (c) 2021, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.common.utils.http.apache;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;

import java.io.IOException;
import java.io.InputStream;

/**
 * 流响应处理器
 *
 * @author Guo Limin
 */
public class InputStreamResponseHandler implements ResponseHandler<InputStream> {
    @Override
    public InputStream handleResponse(HttpResponse response) throws IOException {
        final HttpEntity entity = response.getEntity();
        if (response.getStatusLine().getStatusCode() >= HttpStatus.SC_MULTIPLE_CHOICES) {
            throw new HttpResponseException(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());
        }
        System.err.println(getFileNameFromResponse(response));
        return entity == null ? null : entity.getContent();
    }
    public static String getFileNameFromResponse(HttpResponse response) {
        String fileName = null;
        if (response.containsHeader("Content-Disposition")) {
            String contentDisposition = response.getFirstHeader("Content-Disposition").getValue();
            String[] elements = contentDisposition.split(";");
            for (String element : elements) {
                element = element.trim();
                if (element.startsWith("filename=")) {
                    fileName = element.substring("filename=".length());
                    // Remove surrounding double quotes if they exist
                    if (fileName.startsWith("\"") && fileName.endsWith("\"")) {
                        fileName = fileName.substring(1, fileName.length() - 1);
                    }
                    break;
                }
            }
        }
        return fileName;
    }

}
