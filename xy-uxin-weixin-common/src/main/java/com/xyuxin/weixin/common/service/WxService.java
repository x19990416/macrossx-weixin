/*
 * Copyright (c) 2021, Guo limin(50014792@qq.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.xyuxin.weixin.common.service;

import java.io.IOException;

/**
 * 微信服务统一入口
 *
 * @author Guo Limin
 */
public interface WxService {
    /**
     * 获取微信令牌
     *
     * @return 令牌
     * @throws IOException io异常
     */
    String getAccessToken() throws IOException;
}
